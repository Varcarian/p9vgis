﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

public class SnapNodeCollider : MonoBehaviour
{
    private SnapNodes parent;

	// Use this for initialization
	void Start ()
	{
	    parent = this.transform.parent.GetComponent<SnapNodes>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.name == "SnapNode(Clone)" && parent.isGrabbed)
        {
            transform.parent.position = c.transform.position + (c.transform.localPosition/2);            
        }
    }

    void OnTriggerExit(Collider c)
    {
        parent.trigger = null;
    }
}
