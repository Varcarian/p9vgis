﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ButtonEffects : MonoBehaviour, IPointerEnterHandler, ISelectHandler, IPointerExitHandler
{
    private bool _isPopped = false;
    public void OnPointerEnter(PointerEventData eventData)
    {
        this.transform.localPosition -= new Vector3(0, 0, 15);
        _isPopped = true;
    }
    public void OnSelect(BaseEventData eventData)
    {
        //do your stuff when selected
    }
    public void OnPointerExit(PointerEventData evenData)
    {
        this.transform.localPosition += new Vector3(0, 0, 15);
        _isPopped = false;
    }

    public void OnDisable()
    {
        if (_isPopped)
        {
            this.transform.localPosition += new Vector3(0, 0, 15);
        }
    }

    public void OnEnable()
    {
        _isPopped = false;
    }
}