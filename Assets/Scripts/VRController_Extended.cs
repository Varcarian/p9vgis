﻿using UnityEngine;
using System.Collections;
using Valve.VR;

public class VRController_Extended : MonoBehaviour
{
    public uint controllerIndex;
    public VRControllerState_t controllerState;

    public float PadDistance = 0.8f;

    public bool triggerPressed = false;
    public bool padPressed = false;
    public bool padPressedUp = false;
    public bool padPressedDown = false;
    public bool padPressedLeft = false;
    public bool padPressedRight = false;
    public bool menuPressed = false;
    public bool padTouched = false;

    public event ClickedEventHandler TriggerPressed;
    public event ClickedEventHandler TriggerUnpressed;
    public event ClickedEventHandler PadPressed;
    public event ClickedEventHandler PadUnpressed;
    public event ClickedEventHandler PadPressedUp;
    public event ClickedEventHandler PadUnpressedUp;
    public event ClickedEventHandler PadPressedDown;
    public event ClickedEventHandler PadUnpressedDown;
    public event ClickedEventHandler PadPressedLeft;
    public event ClickedEventHandler PadUnpressedLeft;
    public event ClickedEventHandler PadPressedRight;
    public event ClickedEventHandler PadUnpressedRight;
    public event ClickedEventHandler MenuPressed;
    public event ClickedEventHandler MenuUnpressed;
    public event ClickedEventHandler PadTouched;
    public event ClickedEventHandler PadUntouched;

    public void OnTriggerPressed(ClickedEventArgs e)
    {
        if (TriggerPressed != null)
            TriggerPressed(this, e);
    }

    public void OnTriggerUnpressed(ClickedEventArgs e)
    {
        if (TriggerUnpressed != null)
            TriggerUnpressed(this, e);
    }

    public void OnPadPressed(ClickedEventArgs e)
    {
        if (PadPressed != null)
            PadPressed(this, e);
    }

    public void OnPadUnpressed(ClickedEventArgs e)
    {
        if (PadUnpressed != null)
            PadUnpressed(this, e);
    }

    public void OnPadPressedUp(ClickedEventArgs e)
    {
        if (PadPressedUp != null)
            PadPressedUp(this, e);
    }

    public void OnPadUnpressedUp(ClickedEventArgs e)
    {
        if (PadUnpressedUp != null)
            PadUnpressedUp(this, e);
    }
    public void OnPadPressedDown(ClickedEventArgs e)
    {
        if (PadPressedDown != null)
            PadPressedDown(this, e);
    }

    public void OnPadUnpressedDown(ClickedEventArgs e)
    {
        if (PadUnpressedDown != null)
            PadUnpressedDown(this, e);
    }
    public void OnPadPressedLeft(ClickedEventArgs e)
    {
        if (PadPressedLeft != null)
            PadPressedLeft(this, e);
    }

    public void OnPadUnpressedLeft(ClickedEventArgs e)
    {
        if (PadUnpressedLeft != null)
            PadUnpressedLeft(this, e);
    }
    public void OnPadPressedRight(ClickedEventArgs e)
    {
        if (PadPressedRight != null)
            PadPressedRight(this, e);
    }

    public void OnPadUnpressedRight(ClickedEventArgs e)
    {
        if (PadUnpressedRight != null)
            PadUnpressedRight(this, e);
    }

    public void OnMenuPressed(ClickedEventArgs e)
    {
        if (MenuPressed != null)
            MenuPressed(this, e);
    }

    public void OnMenuUnpressed(ClickedEventArgs e)
    {
        if (MenuUnpressed != null)
            MenuUnpressed(this, e);
    }

    public void OnPadTouched(ClickedEventArgs e)
    {
        if (PadTouched != null)
            PadTouched(this, e);
    }

    public void OnPadUntouched(ClickedEventArgs e)
    {
        if (PadUntouched != null)
            PadUntouched(this, e);
    }

    private bool isInCenter()
    {
        if (controllerState.rAxis0.x < PadDistance && controllerState.rAxis0.x > -PadDistance &&
            controllerState.rAxis0.y < PadDistance && controllerState.rAxis0.y > -PadDistance)
            return true;

        return false;
    }

    // Use this for initialization
    void Start()
    {

        if (controllerIndex != 0)
        {
            if (this.GetComponent<SteamVR_RenderModel>() != null)
            {
                this.GetComponent<SteamVR_RenderModel>().index = (SteamVR_TrackedObject.EIndex)controllerIndex;
            }
        }
        else
        {
            controllerIndex = (uint)this.GetComponent<SteamVR_TrackedObject>().index;
        }
    }

    // Update is called once per frame
    void Update()
    {
        var system = OpenVR.System;
        if (system != null && system.GetControllerState(controllerIndex, ref controllerState,2))//HACK //TODO //SOMETHING
        {
            float trigger = controllerState.rAxis1.x;
            if (trigger >= 1f && !triggerPressed)
            {
                triggerPressed = true;
                ClickedEventArgs e;
                e.controllerIndex = controllerIndex;
                e.flags = (uint)controllerState.ulButtonPressed;
                e.padX = controllerState.rAxis0.x;
                e.padY = controllerState.rAxis0.y;
                OnTriggerPressed(e);

            }
            else if (trigger <= 0.8f && triggerPressed)
            {
                triggerPressed = false;
                ClickedEventArgs e;
                e.controllerIndex = controllerIndex;
                e.flags = (uint)controllerState.ulButtonPressed;
                e.padX = controllerState.rAxis0.x;
                e.padY = controllerState.rAxis0.y;
                OnTriggerUnpressed(e);
            }
        }

        ulong pad = controllerState.ulButtonPressed & (1UL << ((int)EVRButtonId.k_EButton_SteamVR_Touchpad));
        if (pad > 0L && !padPressed && isInCenter())
        {
            padPressed = true;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadPressed(e);
        }
        else if (pad == 0L && padPressed && isInCenter())
        {
            padPressed = false;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadUnpressed(e);
        }

        if (pad > 0L && !padPressedUp && controllerState.rAxis0.y > PadDistance)
        {
            padPressedUp = true;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadPressedUp(e);
        }
        else if (pad == 0L && padPressedUp && controllerState.rAxis0.y > PadDistance)
        {
            padPressedUp = false;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadUnpressedUp(e);
        }

        if (pad > 0L && !padPressedDown && controllerState.rAxis0.y < -PadDistance)
        {
            padPressedDown = true;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadPressedDown(e);
        }
        else if (pad == 0L && padPressedDown && controllerState.rAxis0.y < -PadDistance)
        {
            padPressedDown = false;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadUnpressedDown(e);
        }

        if (pad > 0L && !padPressedLeft && controllerState.rAxis0.x < -PadDistance)
        {
            padPressedLeft = true;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadPressedLeft(e);
        }
        else if (pad == 0L && padPressedLeft && controllerState.rAxis0.x < -PadDistance)
        {
            padPressedLeft = false;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadUnpressedLeft(e);
        }

        if (pad > 0L && !padPressedRight && controllerState.rAxis0.x > PadDistance)
        {
            padPressedRight = true;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadPressedRight(e);
        }
        else if (pad == 0L && padPressedRight && controllerState.rAxis0.x > PadDistance)
        {
            padPressedRight = false;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadUnpressedRight(e);
        }

        ulong menu = controllerState.ulButtonPressed & (1UL << ((int)EVRButtonId.k_EButton_ApplicationMenu));
        if (menu > 0L && !menuPressed)
        {
            menuPressed = true;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnMenuPressed(e);
        }
        else if (menu == 0L && menuPressed)
        {
            menuPressed = false;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnMenuUnpressed(e);
        }

        pad = controllerState.ulButtonTouched & (1UL << ((int)EVRButtonId.k_EButton_SteamVR_Touchpad));
        if (pad > 0L && !padTouched)
        {
            padTouched = true;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadTouched(e);

        }
        else if (pad == 0L && padTouched)
        {
            padTouched = false;
            ClickedEventArgs e;
            e.controllerIndex = controllerIndex;
            e.flags = (uint)controllerState.ulButtonPressed;
            e.padX = controllerState.rAxis0.x;
            e.padY = controllerState.rAxis0.y;
            OnPadUntouched(e);
        }

    }
}

