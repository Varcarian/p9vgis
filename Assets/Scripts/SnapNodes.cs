﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq.Expressions;

public class SnapNodes : MonoBehaviour
{
    private GameObject snapNode;
    private List<Vector3> snapPoints = new List<Vector3>();
    private List<Object> snapNodes = new List<Object>();

    public GameObject trigger;

    public bool snapped = false;
    public bool isGrabbed = false;

    // Use this for initialization
    void Start ()
	{
	    snapNode = Resources.Load("Prefabs/SnapNode") as GameObject;
        snapPoints.Add(new Vector3(0.5f, 0, 0));
        snapPoints.Add(new Vector3(-0.5f, 0, 0));
        snapPoints.Add(new Vector3(0, 0.5f, 0));
        snapPoints.Add(new Vector3(0, -0.5f, 0));
        snapPoints.Add(new Vector3(0, 0, 0.5f));
        snapPoints.Add(new Vector3(0, 0, -0.5f));

	    foreach (var p in snapPoints)
	    {
	        GameObject node = Instantiate(snapNode, p, transform.rotation, transform) as GameObject;
	        node.transform.localPosition = p;
            snapNodes.Add(node);
	    }

    }
	
	// Update is called once per frame
	void Update ()
    {
	    

	}
}
