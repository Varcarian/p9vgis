﻿using UnityEngine;

public static class Vector3Extensions
{
    public static Vector3 Mult(this Vector3 one, Vector3 two)
    {
        return new Vector3(one.x * two.x, one.y * two.y, one.z * two.z);
    }

    public static Vector3 Abs(this Vector3 vec)
    {
        return new Vector3(Mathf.Abs(vec.x), Mathf.Abs(vec.y), Mathf.Abs(vec.z));
    }

    public static Vector3 Divide(this Vector3 one, Vector3 two)
    {
        return new Vector3(one.x / two.x, one.y / two.y, one.z / two.z);
    }

    public static Vector3 Round(this Vector3 vec)
    {
        return new Vector3(Mathf.Round(vec.x), Mathf.Round(vec.y), Mathf.Round(vec.z));
    }

    public static Vector3 InvertBinary(this Vector3 vec)
    {
        if (vec.ToString().Contains("0") || vec.ToString().Contains("1"))
        {
            int x, y, z;
            x = vec.x == 0 ? 1 : 0;
            y = vec.y == 0 ? 1 : 0;
            z = vec.z == 0 ? 1 : 0;
            return new Vector3(x,y,z);
        }
        else
        {
            Debug.Log("Vector containing values other than 1 or 0 used.");
            return vec;
        }        
    }

    public static Vector3 LoopBack(this Vector3 vec)
    {
        Vector3 inter = vec.Abs();
        if (inter.x >= 1f) vec.x = 0;
        if (inter.y >= 1f) vec.y = 0;
        if (inter.z >= 1f) vec.z = 0;
        return vec;
    }

    public static Vector3 WorldToLocal(this Vector3 vec, Transform tForm)
    {
        if (vec == Vector3.forward)
            return tForm.forward;
        if (vec == Vector3.back)
            return tForm.forward*-1;
        if (vec == Vector3.right)
            return tForm.right;
        if (vec == Vector3.left)
            return tForm.right*-1;
        if (vec == Vector3.up)
            return tForm.up;
        if (vec == Vector3.down)
            return tForm.up*-1;
        
        Debug.LogWarning("Vector not recognized, use a cardinal direction!");
        return new Vector3();
    }

}
