﻿Shader "Grids/XZGrid"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_GridStep("Grid size", Float) = 10
		_GridWidth("Grid width", Float) = 1
		_FadeDistance("FadeDistance", Float) = 4.0
		_FocusPoint("FocusPoint", Vector) = (1,1,1,1)
	}
		SubShader
		{
			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 200
			Cull Off
			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
	#pragma surface surf Unlit fullforwardshadows alpha:fade

			// Use shader model 3.0 target, to get nicer looking lighting
	#pragma target 3.0

		sampler2D _MainTex;

		half4 LightingUnlit(SurfaceOutput s, half3 lightDir, half atten) {
			half4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}

		struct Input
		{
			float2 uv_MainTex;
			float3 worldPos;
		};

		fixed4 _Color;
		float _GridStep;
		float _GridWidth;
		float3 _FocusPoint;
		float _FadeDistance;

		void surf(Input IN, inout SurfaceOutput o)
		{
			// Albedo comes from a texture tinted by color
			fixed4 c;
			
			float dist = distance(_FocusPoint, IN.worldPos);
			half opacity = clamp(dist / _FadeDistance, 0, 1);
			// grid overlay
			float2 pos = (IN.worldPos.xz - _GridStep/2) / _GridStep;
			float2 df = fwidth(pos) * _GridWidth;
			float2 g = smoothstep(-df ,df , abs(frac(pos) - 0.5));
			float grid = 1 - saturate(g.x*g.y);
			c.rgb = lerp(float3(0,0,0), _Color.xyz, grid);
			c.a = lerp(0, _Color.w * 1 - opacity , grid);
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
		}
			FallBack "Diffuse"
}
