﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Simple "just colors" shader that's used for built-in debug visualizations,
// in the editor etc. Just outputs _Color * vertex color; and blend/Z/cull/bias
// controlled by material parameters.

Shader "Hidden/GridRender"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_SrcBlend("SrcBlend", Int) = 5.0 // SrcAlpha
		_DstBlend("DstBlend", Int) = 10.0 // OneMinusSrcAlpha
		_ZWrite("ZWrite", Int) = 1.0 // On
		_ZTest("ZTest", Int) = 4.0 // LEqual
		_Cull("Cull", Int) = 0.0 // Off
		_ZBias("ZBias", Float) = 0.0
		_FadeDistance("FaceDistance", Float) = 4.0
		_FocusPoint("FocusPoint", Vector) = (0,0,0,0)
	}

		SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Pass
	{
		Blend[_SrcBlend][_DstBlend]
		ZWrite[_ZWrite]
		ZTest[_ZTest]
		Cull[_Cull]
		Offset[_ZBias],[_ZBias]

		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 2.0
#pragma multi_compile _ STEREO_INSTANCING_ON UNITY_SINGLE_PASS_STEREO 
#include "UnityCG.cginc"

		struct appdata_t {
		float4 vertex : POSITION;
		float4 color : COLOR;
		UNITY_VERTEX_INPUT_INSTANCE_ID
	};
	struct v2f {
		fixed4 color : COLOR;
		float4 vertex : SV_POSITION;
		float4 worldPos : TEXCOORD1;
		UNITY_VERTEX_OUTPUT_STEREO
	};
	float4 _Color;
	v2f vert(appdata_t v)
	{
		v2f o;
		UNITY_SETUP_INSTANCE_ID(v);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.worldPos = mul(unity_ObjectToWorld, v.vertex);

		o.color = v.color * _Color;
		return o;
	}
	float _FadeDistance;
	float4 _FocusPoint;
	fixed4 frag(v2f i) : SV_Target
	{
		fixed4 dist = distance(_FocusPoint, i.worldPos);

		half opacity = clamp(dist / _FadeDistance, 0, 1);

		fixed4 color = fixed4(i.color.xyz * _Color.xyz, 1-opacity);
		return color;
	}
		ENDCG
	}
	}
}
