﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VGIS;

public class VGIS_CraneControlItem : VGIS_InteractableItem
{
    public VGIS_CraneItem Master;

    public override void OnNewPosesApplied()
    {
        base.OnNewPosesApplied();
        //Set line renderer positions and colors
        Master.LineRend.SetPositions(new[] { Master.GetCraneNullPos, Master.GetCraneControlPos });
    }
}
