﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VGIS;
using System.Security.Cryptography;
using Random = UnityEngine.Random;

public class VGIS_Test : MonoBehaviour
{
    static Random _random = new Random();
    public List<GameObject> Points;
    private List<Vector3> _teleportPoints;
    private string _text = "";
    private int _current = 0;
	// Use this for initialization
	void Start ()
	{
	    _teleportPoints = Points.Select(x => x.transform.position).ToList();
        Shuffle(_teleportPoints);
	    MoveToRandomPos(_teleportPoints);
	}

    private void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 50;
        style.normal.textColor = new Color(1.0f, 0.0f, 0f, 1.0f);

        GUI.Label(rect, _text, style);
    }

    // Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MoveToRandomPos(_teleportPoints);
        }	
	}

    public void MoveToRandomPos(List<Vector3> list)
    {
        if (_teleportPoints.Count == 0)
        {
            Debug.Log("Done");
            return;
        }
        int i = Random.Range(0, _teleportPoints.Count-1);
        _current = i;
        Vector3 vec = list[i];
        _text = Points.First(x => x.transform.position == list[i]).name;
        list.RemoveAt(i);
        VGIS_Player.Instance.transform.position = vec;
    }

    public void Shuffle<T>(IList<T> list)
    {
        RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
        int n = list.Count;
        while (n > 1)
        {
            byte[] box = new byte[1];
            do provider.GetBytes(box);
            while (!(box[0] < n * (Byte.MaxValue / n)));
            int k = (box[0] % n);
            n--;
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
