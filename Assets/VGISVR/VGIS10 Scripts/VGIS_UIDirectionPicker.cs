﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VGIS_UIDirectionPicker : MonoBehaviour
{
    private Vector3 _direction;
    public VGIS_ContItem Item;
    public void SetLeft()
    {
        _direction = Vector3.left;
        VGIS_ContItem.CrrntDrctn = _direction;
        Destroy(gameObject);
    }

    public void SetRight()
    {
        _direction = Vector3.right;
        VGIS_ContItem.CrrntDrctn = _direction;
        Destroy(gameObject);        
    }

    public void OnLeftHoverEnter()
    {
        Item.PlaceGhost(Vector3.left);
    }

    public void OnRightHoverEnter()
    {
        Item.PlaceGhost(Vector3.right);
    }

    public void OnHoverExit()
    {
        Item.RemoveGhost();
    }

    private void OnDestroy()
    {
        Item.IsMenuOpen = false;
    }
}
