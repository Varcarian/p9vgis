﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using VGIS;

public class VGIS_GridManager : MonoBehaviour
{
    public static VGIS_GridManager Instance;
    public float SmallStep = 0.1f;
    public float LargeStep = 1;
    public Color SubColor;
    public Color MainColor;
    public float FadeDistance = 2;

    private float _currentFadeDistance = 0;
    private float _fadeInterval = 0.17f;
    private bool _isEnabled;
    private bool _isFading;

    private Material _SmallMaterial;
    private Material _LargeMaterial;
    private Shader _xyShader;
    private Shader _xzShader;
    private Shader _yzShader;

    private Vector3 _focusPoint = new Vector3();
    private Vector3 _direction = (Vector3.zero);

    private GameObject _targetObj;
    private GameObject _gridObject;


    // Use this for initialization
    void Start()
    {
        Instance = this;
        SubColor = Color.red;
        MainColor = Color.green;
        _xyShader = Shader.Find("Grids/XZGrid");
        _xzShader = Shader.Find("Grids/XYGrid");
        _yzShader = Shader.Find("Grids/YZGrid");
        _SmallMaterial = new Material(_xzShader) { color = SubColor };
        _LargeMaterial = new Material(_xzShader) { color = MainColor };

    }

    public void EnableGrid(GameObject obj, float fade = 2)
    {
        _direction = Vector3.zero;
        _targetObj = obj;
        FadeDistance = fade;
        _currentFadeDistance = 0; //Set to zero to not show grid immediatly

        CreateGridObject();
        
        SetShaderProperties();

        _isEnabled = true;
    }

    public void DisableGrid()
    {     
        DestroyImmediate(_gridObject);
        _isEnabled = false;
    }

    public void UpdateGridDirection(Vector3 direction)
    {
        if (!_isEnabled) return;
        _focusPoint = _targetObj.transform.position;

        if (direction != _direction && !_isFading)
        {         
            _isFading = true;
            StartCoroutine(FadeGridSwap(direction));
        }
    }

    private void LateUpdate()
    {
        if (!_isEnabled) return;
        _gridObject.transform.position =
            _focusPoint - _targetObj.GetComponent<VGIS_InteractableItem>().WorldExtents.Mult(_direction)*0.999f*-1;
        SetShaderProperties();
    }

    private IEnumerator FadeGridSwap(Vector3 direction)
    {
        bool isFadedOut = false;
        while (_isFading)
        {
            if (!isFadedOut)
            {
                _currentFadeDistance -= _fadeInterval;
                if (_currentFadeDistance <= 0)
                {
                    _currentFadeDistance = 0;
                    isFadedOut = true;
                }
            }
            else
            {
                _direction = direction;
                SetGridDir();
                _currentFadeDistance += _fadeInterval;
                if (_currentFadeDistance >= FadeDistance)
                {
                    _currentFadeDistance = FadeDistance;
                    _isFading = false;
                }
            }

            yield return new WaitForFixedUpdate();
        }
    }

    private void SetGridDir()
    {
        if (_direction.Abs().x == 1)
        {
            _gridObject.transform.up = _direction * 90;
            SetShaders(_yzShader);
        }
        else if (_direction.Abs().y == 1)
        {
            _gridObject.transform.up = _direction * 90;
            SetShaders(_xyShader);
        }
        else if (_direction.Abs().z == 1)
        {
            _gridObject.transform.up = _direction * 90;
            SetShaders(_xzShader);
        }
    }

    private void SetShaderProperties()
    {
        _SmallMaterial.SetFloat("_FadeDistance", _currentFadeDistance);
        _SmallMaterial.SetVector("_FocusPoint", _focusPoint);
        _SmallMaterial.SetFloat("_GridStep", SmallStep);
        _SmallMaterial.SetFloat("_GridWidth", 1.5f);
        _LargeMaterial.SetFloat("_FadeDistance", _currentFadeDistance);
        _LargeMaterial.SetVector("_FocusPoint", _focusPoint);
        _LargeMaterial.SetFloat("_GridStep", LargeStep);
        _LargeMaterial.SetFloat("_GridWidth", 4.5f);
        _SmallMaterial.color = SubColor;
        _LargeMaterial.color = MainColor;
    }

    private void SetShaders(Shader shader)
    {
        _SmallMaterial.shader = shader;
        _LargeMaterial.shader = shader;
    }

    private void CreateGridObject()
    {
        _gridObject = GameObject.CreatePrimitive(PrimitiveType.Plane);
        _gridObject.name = "GridPlane";
        Destroy(_gridObject.GetComponent<Collider>());
        _gridObject.GetComponent<Renderer>().materials = new List<Material> { _SmallMaterial, _LargeMaterial }.ToArray();
        _gridObject.GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.Off;
        _gridObject.GetComponent<Renderer>().receiveShadows = false;
        _gridObject.transform.position = Vector3.zero;
        _gridObject.transform.rotation = Quaternion.Euler(Vector3.zero);
        _gridObject.transform.localScale = new Vector3(100, 100, 100);
    }



}
