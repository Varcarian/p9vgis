﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VGIS;

[RequireComponent(typeof(BoxCollider))]
public class VGIS_ContItem : VGIS_InteractableItem
{
    private static VGIS_ContItem _crrntBaseItm = null;
    private static Vector3 _crrntDrctn = Vector3.left;
    public static int FloorHeight = 0;

    private GameObject _ghostObject;
    private bool _ghostEnabled = false;
    private bool _canPlace = false;
    public bool IsMenuOpen = false;

    public static Vector3 CrrntDrctn
    {
        get { return _crrntDrctn; }
        set { _crrntDrctn = value; }
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        Rigidbody.freezeRotation = true;
        Rigidbody.isKinematic = true;
        DisableKinematicOnAttach = true;
        EnableKinematicOnDetach = true;
        SetupGhost();
        AdjustCollider();
    }

    IEnumerator ShowGhost()
    {
        _ghostObject.SetActive(true);
        _ghostObject.transform.localScale = this.transform.localScale * 0.999f;
        while (_ghostEnabled)
        {
            
            if (_crrntBaseItm != null)
            {
                Vector3 closeExts = _crrntBaseItm.WorldExtents;
                Vector3 ext = WorldExtents;
                Vector3 dir = _crrntDrctn;
                if (_crrntBaseItm.transform.InverseTransformDirection(_crrntDrctn).Abs() == Vector3.forward)
                {
                    dir = _crrntDrctn*-1;
                    ext = new Vector3(ext.z, ext.y, ext.x);
                    closeExts = new Vector3(closeExts.z, closeExts.y, closeExts.x);
                }
                _ghostObject.transform.position = _crrntBaseItm.transform.position +
                                                  _crrntBaseItm.transform.InverseTransformDirection(dir*-1)
                                                      .Mult(closeExts + ext) +
                                                  _crrntBaseItm.transform.up.Mult(closeExts * -1 + ext) + // Move to floor
                                                  _crrntBaseItm.transform.forward.Mult(closeExts * -1 + ext); // Move to wall

                _ghostObject.transform.rotation = _crrntBaseItm.transform.rotation;
            }

            int layerMask = 1 << LayerMask.NameToLayer("CurrentBox");
            layerMask = ~layerMask;
            Collider[] colliders = Physics.OverlapBox(_ghostObject.transform.position, Bounds.extents * 0.995f, _ghostObject.transform.rotation, layerMask);
            if (colliders.Length > 0)
            {
                foreach (MeshRenderer child in _ghostObject.GetComponentsInChildren<MeshRenderer>())
                {
                    foreach (Material material in child.materials)
                    {
                        material.color = new Color(1, 0, 0, 0.5f);
                    }
                }
                _ghostObject.transform.localScale = this.transform.localScale * 1.001f;
                _canPlace = false;
            }
            else
            {
                foreach (MeshRenderer child in _ghostObject.GetComponentsInChildren<MeshRenderer>())
                {
                    foreach (Material material in child.materials)
                    {
                        material.color = new Color(0, 1, 0, 0.5f);
                    }
                }
                _ghostObject.transform.localScale = this.transform.localScale * 0.999f;
                _canPlace = true;
            }
            yield return new WaitForFixedUpdate();
        }
        _ghostObject.SetActive(false);
    }

    void SetupGhost()
    {
        _ghostObject = new GameObject(name + " : Ghost");
        _ghostObject.SetActive(false);

        MeshRenderer[] childs = GetComponentsInChildren<MeshRenderer>();

        foreach (var child in childs)
        {
            GameObject newChild = new GameObject(child.gameObject.name + " : GhostPart");
            newChild.transform.SetParent(_ghostObject.transform);
            newChild.AddComponent<MeshFilter>().mesh = child.transform.GetComponent<MeshFilter>().mesh;
            newChild.AddComponent<MeshRenderer>().materials = child.materials;

            foreach (Material mat in newChild.GetComponent<MeshRenderer>().materials)
            {
                mat.SetFloat("_Mode", 3.0f);
                mat.shader = Shader.Find("Legacy Shaders/Transparent/VertexLit");
                mat.color = new Color(0, 1, 0, 0.5f);
                //newChild.transform.localPosition = child.transform.localPosition;
                newChild.transform.localPosition = child.name == transform.name ? Vector3.zero : child.transform.localPosition; //TODO: In case of bug check this ( name is the same in the game it's a shame)
                newChild.transform.localRotation = child.transform.localRotation;
            }
        }
        _ghostObject.transform.localScale = transform.localScale * 0.999f;
    }

    private bool PlaceItem()
    {
        if (!_canPlace) return false;
        transform.position = _ghostObject.transform.position;
        transform.rotation = _ghostObject.transform.rotation;
        return true;
    }

    public void PlaceGhost(Vector3 dir)
    {
        _ghostEnabled = true;
        _crrntDrctn = dir;
        StartCoroutine(ShowGhost());
    }

    public void RemoveGhost()
    {
        _ghostEnabled = false;
        StopCoroutine(ShowGhost());
    }

    public override bool BeginSelect(VGIS_Interaction hand)
    {
        if (!CanSelect) return false;

        if (_crrntBaseItm == null)
        {
            _crrntBaseItm = this;
            IsSelected = true;
            Hightlight(hand);
            Selectinghand = hand;

            //Stuff for menus

            GetComponent<Rigidbody>().useGravity = false;
            //Create menu
            Menu = Instantiate(Resources.Load("DirectionPickerMenu") as GameObject);
            Menu.transform.SetParent(transform);
            Menu.GetComponent<VGIS_UIDirectionPicker>().Item = this;
            Menu.GetComponent<Canvas>().worldCamera = VGIS_ViveControllerInput.Instance.ControllerCamera;
            Vector3[] corners = new Vector3[4];
            Menu.GetComponent<RectTransform>().GetWorldCorners(corners);

            //float dot = Vector3.Dot(VGIS_Player.Instance.Head.transform.forward, transform.forward);

            //if (dot > 0)
            //{
            //    Menu.transform.position = transform.position - transform.forward.Mult(WorldExtents + new Vector3(0.01f, 0.01f, 0.01f));
            //    Menu.transform.LookAt(Menu.transform.position + transform.forward*2);
            //}
            //else
            //{
            //    Menu.transform.position = transform.position + transform.forward.Mult(WorldExtents + new Vector3(0.01f, 0.01f, 0.01f));
            //    Menu.transform.LookAt(Menu.transform.position - transform.forward * 2);
            //}

            Menu.transform.position = transform.position + transform.forward.Mult(WorldExtents + new Vector3(0.01f, 0.01f, 0.01f));
            Menu.transform.LookAt(Menu.transform.position - transform.forward * 2);
            IsMenuOpen = true;
            return true;
        }
        if (_crrntBaseItm == this)
        {
            return false;
        }
        else
        {
            if (!PlaceItem()) return false;

            _crrntBaseItm.EndSelect(null);
            IsSelected = true;
            Hightlight(hand);
            Selectinghand = hand;
            _crrntBaseItm = this;
            return true;
        }
    }

    public override bool EndSelect(VGIS_Interactable newItem)
    {
        if ((_crrntBaseItm == this) && (newItem != null) && (newItem.GetType() == typeof(VGIS_ContItem))) return false;

        _crrntBaseItm = null;
        return base.EndSelect(newItem);
    }

    public override void StartHoverHightlight(VGIS_Interaction hand, Color color)
    {
        if (_crrntBaseItm != null && _crrntBaseItm != this)
        {
            _ghostEnabled = true;
            StartCoroutine(ShowGhost());
        }

        base.StartHoverHightlight(hand, color);
    }

    public override bool StopHoverHightlight(VGIS_Interaction hand, VGIS_Interactable newItem = null)
    {
        if (!IsMenuOpen)
        {
            _ghostEnabled = false;
            StopCoroutine(ShowGhost());
        }    
        base.StopHoverHightlight(hand);

        return true;
    }
}
