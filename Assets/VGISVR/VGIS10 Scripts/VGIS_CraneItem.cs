﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration.Assemblies;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using Valve.VR;
using VGIS;
public class VGIS_CraneItem : VGIS_InteractableItem
{
    public float GridSize = 0.1f;
    public bool IsAxisLocked = true;
    private float _roundingDecimal = 0.5f;

    private GameObject _craneNull;
    private GameObject _craneControl;
    private GameObject _movementTarget;
    private LineRenderer _lineRenderer;
    private bool _hasMoved = false;
    private float _controlDistance;
    private float _speed;
    private GameObject _directionArrow;

    public Vector3 GetCraneNullPos
    {
        get { return _craneNull.transform.position; }
    }

    public Vector3 GetCraneControlPos
    {
        get { return _craneControl.transform.position; }
    }

    public LineRenderer LineRend
    {
        get { return _lineRenderer; }
        set { _lineRenderer = value; }
    }

    protected override void Start()
    {
        base.Start();
        Rigidbody.freezeRotation = true;
        Rigidbody.isKinematic = true;
        CanAttach = false;
        AdjustCollider();
        InitLine();
    }

    protected override void Update()
    {
        base.Update();
        if (GridSize > 0)
        {
            _roundingDecimal = 1 / GridSize;
        }

        if (IsSelected)
        {
            VGIS_GridManager.Instance.SmallStep = GridSize;//Update grid size if changed in inspector
            MoveItem(); //Move the item and update the color of the indicator
        }
    }

    private void MoveItem()
    {
        //Reset crane control
        if (!_craneControl.GetComponent<VGIS_InteractableItem>().IsAttached)
        {
            _craneNull.transform.position = _craneControl.transform.position;
            _craneControl.transform.localPosition = Vector3.zero;
            _lineRenderer.SetPositions(new[] { _craneNull.transform.position, _craneControl.transform.position });
            _directionArrow.transform.localScale = Vector3.zero;
            return;
        }

        //Calculate distance and speed
        _controlDistance = Vector3.Distance(_craneNull.transform.position, _craneControl.transform.position);
        _speed = Mathf.Min(0.005f,_controlDistance * Time.deltaTime);

        //Calculate the adjusted movement vector based on closest axis
        Vector3 adjMovVec = new Vector3();
        List<float> dots = new List<float>
        {
            Vector3.Dot(_craneNull.transform.up.normalized, _craneControl.transform.localPosition.normalized.Abs()), // Y Axis
            Vector3.Dot(_craneNull.transform.right.normalized, _craneControl.transform.localPosition.normalized.Abs()), // X Axis
            Vector3.Dot(_craneNull.transform.forward.normalized, _craneControl.transform.localPosition.normalized.Abs()), // Z Axis
            Vector3.Dot((_craneNull.transform.right + _craneNull.transform.forward).normalized, _craneControl.transform.localPosition.normalized.Abs()), // XZ plane
            Vector3.Dot((_craneNull.transform.right + _craneNull.transform.up).normalized, _craneControl.transform.localPosition.normalized.Abs()), // XY plane
            Vector3.Dot((_craneNull.transform.up + _craneNull.transform.forward).normalized, _craneControl.transform.localPosition.normalized.Abs()), // YZ plane
        };

        float maxValue = dots.Max();

        if (IsAxisLocked)
        {
            switch (dots.IndexOf(maxValue))
            {
                case 0:
                    adjMovVec = _craneControl.transform.localPosition.Mult(Vector3.up);
                    _lineRenderer.endColor = Color.green;
                    _lineRenderer.startColor = Color.green;
                    break;
                case 1:
                    adjMovVec = _craneControl.transform.localPosition.Mult(Vector3.right);
                    _lineRenderer.endColor = Color.red;
                    _lineRenderer.startColor = Color.red;
                    break;
                case 2:
                    adjMovVec = _craneControl.transform.localPosition.Mult(Vector3.forward);
                    _lineRenderer.endColor = Color.blue;
                    _lineRenderer.startColor = Color.blue;
                    break;
                case 3:
                    adjMovVec = _craneControl.transform.localPosition.Mult(Vector3.right + Vector3.forward);
                    _lineRenderer.endColor = Color.red;
                    _lineRenderer.startColor = Color.blue;
                    break;
                case 4:
                    adjMovVec = _craneControl.transform.localPosition.Mult(Vector3.right + Vector3.up);
                    _lineRenderer.endColor = Color.red;
                    _lineRenderer.startColor = Color.green;
                    break;
                case 5:
                    adjMovVec = _craneControl.transform.localPosition.Mult(Vector3.up + Vector3.forward);
                    _lineRenderer.endColor = Color.green;
                    _lineRenderer.startColor = Color.blue;
                    break;
            }
        }
        else
        {
            adjMovVec = _craneControl.transform.localPosition;
            Vector3 normPos = adjMovVec.normalized.Abs();
            var lineCol = new Color(normPos.x, normPos.y, normPos.z);
            _lineRenderer.endColor = lineCol;
            _lineRenderer.startColor = lineCol;
        }

        CalculateGridDirection(adjMovVec);


        //Update direction arrow gizmo 
        _directionArrow.transform.rotation = Quaternion.LookRotation(_craneControl.transform.localPosition);
        _directionArrow.transform.localScale = new Vector3(10, 15, _controlDistance * 25);
        //Move the target that the actual object follows
        _movementTarget.transform.position += adjMovVec * _speed;

        //Move the actual object, if it has moved then trigger haptic
        Vector3 oldPos = transform.position;
        Vector3 newPos = (_movementTarget.transform.position * _roundingDecimal).Round() / _roundingDecimal;

        int layerMask = 1 << LayerMask.NameToLayer("CurrentBox");
        layerMask = ~layerMask;
        if(!Physics.CheckBox(newPos, WorldExtents * 0.99f, transform.rotation, layerMask))
        {
            transform.position = newPos;
        }
        else
        {
            _movementTarget.transform.position = transform.position;
        }
        

        if (oldPos != transform.position)
        {
            _hasMoved = true;
            Selectinghand.controllerActions.TriggerHapticPulse(999);
        }
        else _hasMoved = false;

    }

    private void CalculateGridDirection(Vector3 movementVector)
    {
        if (!_hasMoved) return;
        //Store direction vectors
        Dictionary<Vector3, float> directions = new Dictionary<Vector3, float>
        {
            { transform.right.normalized, 0},
            { transform.up.normalized, 0},
            { transform.forward.normalized, 0},
            { transform.right.normalized * -1, 0},
            { transform.up.normalized * -1, 0},
            { transform.forward.normalized * -1, 0}
        };

        //Get the forward vector of the camera
        Vector3 view = VGIS_Player.Instance.Head.transform.forward;

        //Calculate dot products
        foreach (Vector3 dir in directions.Keys.ToList())
        {
            float viewProduct = Vector3.Dot(dir, view);
            
            // if it is above zero it means its on one of the opposite sides and should be kept
            if (viewProduct > 0)
            {                
                float directionProduct = Vector3.Dot(dir.Abs().normalized, movementVector.Abs().normalized);

                directions[dir] = viewProduct - (directionProduct * 2); // *2 to prioritize move direction over view
            }
            //otherwise removed            
            else
                directions.Remove(dir);
        }
        //Sort the list and get the best value
        float bestScore = directions.Values.OrderBy(x => x).Last();
        //Get the key with the highest score
        Vector3 finalVec = directions.FirstOrDefault(x => x.Value == bestScore).Key;

        //Update the grid
        VGIS_GridManager.Instance.UpdateGridDirection(finalVec);    
    }

    private void InitLine()
    {
        _lineRenderer = gameObject.AddComponent<LineRenderer>();
        _lineRenderer.shadowCastingMode = ShadowCastingMode.Off;
        _lineRenderer.receiveShadows = false;
        _lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        _lineRenderer.startWidth = 0.05f;
        _lineRenderer.endWidth = 0.099f;
        _lineRenderer.numPositions = 2;
        _lineRenderer.numCapVertices = 10;
        _lineRenderer.useWorldSpace = true;
        _lineRenderer.enabled = false;
    }

    public override bool BeginSelect(VGIS_Interaction hand)
    {
        if (hand == VGIS_Player.Instance.LeftHand) return false;
        if (!base.BeginSelect(hand)) return false;
        VGIS_GridManager.Instance.EnableGrid(this.gameObject);
        ShowCraneControl();
        CreateDirectionArrow();
        hand.ControlLaser();
        transform.gameObject.layer = LayerMask.NameToLayer("CurrentBox");
        return true;
    }

    public override bool EndSelect(VGIS_Interactable newItem)
    {
        Destroy(_directionArrow);
        HideCraneControl();
        VGIS_GridManager.Instance.DisableGrid();

        transform.gameObject.layer = LayerMask.NameToLayer("Interactable");
        base.EndSelect(newItem);
        return true;
    }

    private void ShowCraneControl()
    {
        _craneNull = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        _craneNull.name = "_CraneNullpoint";
        _craneNull.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        _craneNull.transform.position = Selectinghand.transform.position;
        _craneNull.transform.rotation = new Quaternion();

        _craneControl = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        _craneControl.name = "_craneControl";
        VGIS_CraneControlItem item = _craneControl.AddComponent<VGIS_CraneControlItem>();
        item.CanSelect = false;
        item.Rigidbody.freezeRotation = true;
        item.Rigidbody.isKinematic = true;
        item.DisableKinematicOnAttach = false;
        item.EnableKinematicOnDetach = true;
        item.EnableGravityOnDetach = false;
        item.CanHoverHightlight = false;
        item.Master = this;
        _craneControl.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        _craneControl.transform.position = _craneNull.transform.position;
        _craneControl.transform.rotation = _craneNull.transform.rotation;
        _craneControl.transform.SetParent(_craneNull.transform);

        _movementTarget = new GameObject("_movementTarget");
        _movementTarget.transform.position = transform.position;

        _lineRenderer.enabled = true;
    }

    private void HideCraneControl()
    {
        Destroy(_craneNull);
        Destroy(_craneControl);
        Destroy(_movementTarget);
        _lineRenderer.enabled = false;
    }

    private void CreateDirectionArrow()
    {
        _directionArrow = Instantiate(Resources.Load("DirectionArrow") as GameObject);
        _directionArrow.transform.localScale = Vector3.zero;
        _directionArrow.transform.position = transform.position;
        _directionArrow.transform.forward = this.transform.forward;
        _directionArrow.transform.SetParent(transform);
        _directionArrow.transform.localEulerAngles = Vector3.zero;
    }
}
