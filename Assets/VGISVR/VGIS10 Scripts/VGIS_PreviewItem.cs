﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VGIS;

public class VGIS_PreviewItem : VGIS_InteractableItem
{
    public static VGIS_PreviewItem CrrntlyPlcngItem;
    private List<GhostInterface> _ghostInterfaces = new List<GhostInterface>();
    private GameObject _ghostObject;
    public static List<GameObject> Ghosts = new List<GameObject>();

    public List<GhostInterface> GhostInterfaces
    {
        get { return _ghostInterfaces; }
    }

    public GameObject GhostObject
    {
        get { return _ghostObject; }
    }

    protected override void Awake()
    {
        base.Awake();
        SetupGhost();

        AddGhostInterface(Vector3.left, Vector3.forward);
        AddGhostInterface(Vector3.right, Vector3.forward);

    }

    protected override void Start()
    {
        base.Start();
        Rigidbody.freezeRotation = true;
        Rigidbody.isKinematic = true;
        DisableKinematicOnAttach = true;
        EnableKinematicOnDetach = true;
        AdjustCollider();
    }

    public static void DestroyGhosts()
    {
        if (Ghosts.Count == 0) return;
        foreach (GameObject ghost in Ghosts)
        {
            Destroy(ghost);
        }
        Ghosts.Clear();      
    }

    public override void StartHoverHightlight(VGIS_Interaction hand, Color color)
    {
        if(CrrntlyPlcngItem != null && CrrntlyPlcngItem != this && Ghosts.Count == 0)
            ShowGhosts();

        base.StartHoverHightlight(hand, color);   
    }

    public override bool StopHoverHightlight(VGIS_Interaction hand, VGIS_Interactable newItem = null)
    {
        if (newItem == null || newItem.GetType() != typeof(VGIS_PreviewGhost))
        {
            DestroyGhosts();
        }
        
        base.StopHoverHightlight(hand);
        return true;
    }

    public override bool BeginSelect(VGIS_Interaction hand)
    {
        if (this == CrrntlyPlcngItem) return false;

        if (CrrntlyPlcngItem != null)
        {
            DestroyGhosts();
        }

        CrrntlyPlcngItem = this;
        gameObject.layer = LayerMask.NameToLayer("CurrentBox");
        return base.BeginSelect(hand);
    }

    public override bool EndSelect(VGIS_Interactable newItem)
    {
        if (newItem == null)
        {
            gameObject.layer = LayerMask.NameToLayer("Interactable");
            CrrntlyPlcngItem = null;
        }
            

        if (newItem == null || newItem.GetType() != typeof(VGIS_PreviewGhost)) return base.EndSelect(newItem);

        var item = (VGIS_PreviewGhost) newItem;
        if (item.CanPlace)
        {
            gameObject.layer = LayerMask.NameToLayer("Interactable");
            return base.EndSelect(newItem);
        }
            
        return false;
    }

    /// <summary>
    /// Adds a ghost interface to the list of availible interfaces for this item.
    /// </summary>
    /// <param name="dir">The direction in which the interface is facing.</param>
    /// <param name="defaultForward">The default direction an item placed at this interface will consider as forward.</param>
    /// <returns>True if interface successfully added, otherwise false.</returns>
    public bool AddGhostInterface(Vector3 dir, Vector3 defaultForward)
    {
        GhostInterface gI = new GhostInterface(dir, defaultForward);

        if (GhostInterfaces.Contains(gI))
        {
            Debug.LogError("Ghost interface already exists!");
            return false;
        }
        GhostInterfaces.Add(gI);
        return true;
    }

    private void SetupGhost()
    {
        _ghostObject = new GameObject(name + " : Ghost");
        GhostObject.SetActive(false);
        GhostObject.hideFlags = HideFlags.HideInHierarchy;

        MeshRenderer[] childs = GetComponentsInChildren<MeshRenderer>();

        foreach (var child in childs)
        {
            GameObject newChild = new GameObject(child.gameObject.name + " : GhostPart");
            newChild.transform.SetParent(GhostObject.transform);
            newChild.AddComponent<MeshFilter>().mesh = child.transform.GetComponent<MeshFilter>().mesh;
            newChild.AddComponent<MeshRenderer>().materials = child.materials;

            foreach (Material mat in newChild.GetComponent<MeshRenderer>().materials)
            {
                mat.SetFloat("_Mode", 3.0f);
                mat.shader = Shader.Find("Legacy Shaders/Transparent/VertexLit");
                mat.color = new Color(0, 1, 0, 0.5f);
                //newChild.transform.localPosition = child.transform.localPosition;
                newChild.transform.localPosition = child.name == transform.name ? Vector3.zero : child.transform.localPosition; //TODO: In case of bug check this ( name is the same in the game it's a shame)
                newChild.transform.localRotation = child.transform.localRotation;
            }
        }
        GhostObject.AddComponent<BoxCollider>();
        GhostObject.AddComponent<VGIS_PreviewGhost>();
        GhostObject.transform.localScale = transform.localScale * 0.999f;
    }

    private void ShowGhosts()
    {
        foreach (GhostInterface ghstIntrfc in GhostInterfaces)
        {           
            GameObject newGhost = Instantiate(CrrntlyPlcngItem.GhostObject);
            Ghosts.Add(newGhost);

            Quaternion orgRot = CrrntlyPlcngItem.transform.rotation;
            CrrntlyPlcngItem.transform.rotation = transform.rotation;
            Vector3 closeExts = CrrntlyPlcngItem.GetComponent<Collider>().bounds.extents;
            CrrntlyPlcngItem.transform.rotation = orgRot;

            Vector3 ext = GetComponent<Collider>().bounds.extents;
            Vector3 totalExts = closeExts + ext;
            Vector3 dir = ghstIntrfc.Direction;

            if (dir.Abs() == Vector3.right)
            {
                newGhost.transform.position = transform.position + dir.WorldToLocal(transform).Mult(totalExts) + //Move "out" from center
                                              transform.up.Mult(closeExts + ext * -1) + // Move to floor
                                              transform.forward.Mult(closeExts + ext * -1); // Move to wall
            }
            else if (dir.Abs() == Vector3.forward)
            {
                newGhost.transform.position = transform.position + dir.WorldToLocal(transform).Mult(totalExts) + //Move "out" from center
                                              transform.up.Mult(closeExts + ext * -1);
            }
            else
            {
                newGhost.transform.position = transform.position + dir.WorldToLocal(transform).Mult(totalExts) + //Move "out" from center
                                              transform.forward.Mult(closeExts + ext * -1);
            }
        
            newGhost.transform.rotation = Quaternion.LookRotation(ghstIntrfc.DefaultFrwdDir.WorldToLocal(transform),Vector3.up);
            newGhost.GetComponent<VGIS_PreviewGhost>().Parent = this;
            newGhost.SetActive(true);
        }      
    }

    public class GhostInterface
    {
        #region Variables

        private readonly Vector3 _direction;
        private readonly Vector3 _defaultFrwdDir;
        private bool _isOccupied;

        #endregion

        #region Properties

        public Vector3 DefaultFrwdDir
        {
            get { return _defaultFrwdDir; }
        }

        public Vector3 Direction
        {
            get { return _direction; }
        }

        public bool IsOccupied
        {
            get { return _isOccupied; }
            set { _isOccupied = value; }
        }

        #endregion

        #region Constructors

        public GhostInterface(Vector3 direction)
        {
            _direction = direction;
            _defaultFrwdDir = Vector3.forward;
            IsOccupied = false;
        }
        public GhostInterface(Vector3 direction, Vector3 defaultForward)
        {
            _direction = direction;
            _defaultFrwdDir = defaultForward;
            IsOccupied = false;
        }

        #endregion

        public override bool Equals(System.Object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            GhostInterface gI = (GhostInterface) obj;
            return _direction == gI._direction;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = _direction.GetHashCode();
                return hashCode;
            }
        }
    }
}
