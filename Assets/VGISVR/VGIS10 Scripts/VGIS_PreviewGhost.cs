﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VGIS;

public class VGIS_PreviewGhost : VGIS_InteractableItem
{
    public VGIS_PreviewItem Parent;
    private bool _canPlace = false;
    private Vector3 _orgScale;
    private float _ghostAlpha = 0.5f;

    public bool CanPlace
    {
        get { return _canPlace; }
    }

    protected override void Start()
    {
        base.Start();
        Rigidbody.freezeRotation = true;
        Rigidbody.isKinematic = true;
        DisableKinematicOnAttach = true;
        EnableKinematicOnDetach = true;
        AdjustCollider();
        _orgScale = transform.localScale;
    }
    
    
    protected override void Update()
    {
        gameObject.layer = LayerMask.NameToLayer("CurrentBox");
        int layerMask = 1 << LayerMask.NameToLayer("CurrentBox");
        layerMask = ~layerMask;

        Collider[] colliders = Physics.OverlapBox(transform.position, GetComponent<Collider>().bounds.extents * 0.995f, Quaternion.Euler(0,0,0), layerMask);
        if (colliders.Length > 0)
        {
            foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
            {
                foreach (Material material in child.materials)
                {
                    material.color = new Color(1, 0, 0, _ghostAlpha);
                }
            }
            transform.localScale = _orgScale * 1.001f;
            _canPlace = false;
        }
        else
        {
            foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
            {
                foreach (Material material in child.materials)
                {
                    material.color = new Color(0, 1, 0, _ghostAlpha);
                }
            }
            transform.localScale = _orgScale * 0.999f;
            _canPlace = true;
        }
        
    }

    public override void StartHoverHightlight(VGIS_Interaction hand, Color color)
    {
        if (IsHoverHightlighted) return;
        if (!CanHoverHightlight) return;
        HoveringHand = hand;
        if (this.GetComponent<VGIS_SnapManager>())
        {
            foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
            {
                if (part.Value == false) continue;
                if (part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted) continue;
                part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted = true;
                part.Key.GetComponent<VGIS_Interactable>().HoveringHand = hand;
                _listOfHoverMaterials.AddRange(part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.sharedMaterials);
                foreach (Material hoverMaterial in _listOfHoverMaterials)
                {
                    hoverMaterial.EnableKeyword("_EMISSION");
                    hoverMaterial.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
                }
            }
        }
        else
        {
            if (IsHoverHightlighted) return;
            HoveringHand = hand;

            foreach (var render in GetComponentsInChildren<MeshRenderer>())
            {
                _listOfHoverMaterials.AddRange(render.materials);

                foreach (Material material in render.materials)
                {
                    material.EnableKeyword("_EMISSION");
                    material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
                }
            }
        }
        HighligtherEnumerator = HoverHightlighter(_listOfHoverMaterials, color);
        IsHoverHightlighted = true;
        StartCoroutine(HighligtherEnumerator);
        _ghostAlpha = 1f;
    }

    public override bool StopHoverHightlight(VGIS_Interaction hand, VGIS_Interactable newItem = null)
    {
        if (newItem == null || newItem != Parent)
        {
            VGIS_PreviewItem.DestroyGhosts();
        }
        
        if (!IsHoverHightlighted) return false;
        if (HoveringHand != hand) return false;

        if (this.GetComponent<VGIS_SnapManager>())
        {
            foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
            {
                if (part.Value == false) continue;
                if (!part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted) continue;
                if (part.Key.GetComponent<VGIS_Interactable>().HoveringHand != hand) continue;
                part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted = false;
                part.Key.GetComponent<VGIS_Interactable>().HoveringHand = null;
                foreach (Material hoverMaterial in _listOfHoverMaterials)
                {
                    hoverMaterial.DisableKeyword("_EMISSION");
                }
            }
        }
        else
        {
            foreach (Material hoverMaterial in _listOfHoverMaterials)
            {
                hoverMaterial.DisableKeyword("_EMISSION");
                hoverMaterial.globalIlluminationFlags = MaterialGlobalIlluminationFlags.EmissiveIsBlack;
            }
        }
        IsHoverHightlighted = false;
        HoveringHand = null;
        StopCoroutine(HighligtherEnumerator);
        _listOfHoverMaterials.Clear();
        _ghostAlpha = 0.5f;
        return true;
    }

    public override bool BeginSelect(VGIS_Interaction hand)
    {
        if (!CanPlace) return false;
        VGIS_PreviewItem.CrrntlyPlcngItem.transform.position = transform.position;
        VGIS_PreviewItem.CrrntlyPlcngItem.transform.rotation = transform.rotation;
        VGIS_PreviewItem.CrrntlyPlcngItem = null;
        VGIS_PreviewItem.DestroyGhosts();
        return false;
    }
}
