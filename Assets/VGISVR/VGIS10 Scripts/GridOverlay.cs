﻿using System;
using UnityEngine;
using System.Collections;
using VGIS;
using VRTK.Examples.Utilities;

public class GridOverlay : MonoBehaviour
{
    public static GridOverlay Instance;
    //public GameObject plane;
    private bool _isEnabled = false;
    public bool showMain = true;
    public bool showSub = false;

    public int gridSizeX;
    public int gridSizeY;
    public int gridSizeZ;

    public float smallStep;
    public float largeStep;

    public float startX;
    public float startY;
    public float startZ;

    private Material lineMaterial;

    public Color mainColor = new Color(0f, 1f, 0f, 1f);
    public Color subColor = new Color(0f, 0.5f, 0f, 1f);


    public Vector4 FocusPoint = Vector4.zero;

    private GameObject _targetObj;
    public Vector3 OrgPos;
    private Vector3 _oldPos;
    private Vector3 _direction;
    private Vector3 _movCount = new Vector3();

    private bool _isFading;
    private float _fadeInterval = 0.17f;
    private float _currentFadeDistance;
    public float FadeDistance = 4;

    private void Start()
    {
        Instance = this;
    }

    void CreateLineMaterial()
    {       
        if (!lineMaterial)
        {           
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            //var shader = Shader.Find("Hidden/Internal-Colored");
            var shader = Shader.Find("Hidden/GridRender");//Use custom shader for fading the grid
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
        lineMaterial.SetFloat("_FadeDistance", _currentFadeDistance);
        lineMaterial.SetVector("_FocusPoint", FocusPoint);
    }

    public void EnableGrid(GameObject obj, Vector3 direction, float fade = 4)
    {
        _targetObj = obj;
        gridSizeX = direction.Abs().x == 1 ? 0 : 7;
        gridSizeY = direction.Abs().y == 1 ? 0 : 7;
        gridSizeZ = direction.Abs().z == 1 ? 0 : 7;
        _direction = direction;
        FadeDistance = fade;
        _currentFadeDistance = 0; //Set to zero to not show grid immediatly
        _oldPos = OrgPos;
        _isEnabled = true;       
    }

    public void DisableGrid()
    {
        _isEnabled = false;
    }

    public void UpdateGridDirection(Vector3 direction)
    {
        if (direction != _direction && !_isFading)
        {
            _isFading = true;
            StartCoroutine(FadeGridSwap(direction));
        }
    }

    private IEnumerator FadeGridSwap(Vector3 direction)
    {
        bool isFadedOut = false;
        while (_isFading)
        {
            if (!isFadedOut)
            {
                _currentFadeDistance -= _fadeInterval;
                if (_currentFadeDistance <= 0)
                {
                    _currentFadeDistance = 0;
                    isFadedOut = true;
                }
            }
            else
            {
                _direction = direction;
                gridSizeX = direction.Abs().x == 1 ? 0 : 7;
                gridSizeY = direction.Abs().y == 1 ? 0 : 7;
                gridSizeZ = direction.Abs().z == 1 ? 0 : 7;

                _currentFadeDistance += _fadeInterval;
                if (_currentFadeDistance >= FadeDistance)
                {
                    _currentFadeDistance = FadeDistance;
                    _isFading = false;
                }              
            }
            yield return new WaitForFixedUpdate();
        }
    }

    void OnPostRender()
    {
        if (!_isEnabled) return;
        if (smallStep < 0.1f || largeStep < 0.1f) return;
        FocusPoint = _targetObj.transform.position;

        //Add to count when the object is moved
        if (FocusPoint.x > _oldPos.x)
            _movCount.x += smallStep;
        else if (FocusPoint.x < _oldPos.x)        
            _movCount.x -= smallStep;
        if (FocusPoint.y > _oldPos.y)
            _movCount.y += smallStep;
        else if (FocusPoint.y < _oldPos.y)
            _movCount.y -= smallStep;
        if (FocusPoint.z > _oldPos.z)
            _movCount.z += smallStep;
        else if (FocusPoint.z < _oldPos.z)
            _movCount.z -= smallStep;

        //Loop the vector, if value is one or higher, set it back to zero
        _movCount = _movCount.LoopBack();

        //Use direction to determine which surface of object to drawn grid along
        var offsetDirX = _direction.x != 0 ? _direction.x * -1 : 1;
        var offsetDirY = _direction.y != 0 ? _direction.y * -1 : 1;
        var offsetDirZ = _direction.z != 0 ? _direction.z * -1 : 1;

        //Center the grid on the object       
        startX = FocusPoint.x - gridSizeX/2 - _targetObj.GetComponent<VGIS_InteractableItem>().WorldExtents.x * offsetDirX;
        startY = FocusPoint.y - gridSizeY/2 - _targetObj.GetComponent<VGIS_InteractableItem>().WorldExtents.y * offsetDirY;
        startZ = FocusPoint.z - gridSizeZ/2 - _targetObj.GetComponent<VGIS_InteractableItem>().WorldExtents.z * offsetDirZ;

        //Add the offset from movement
        startX -= _movCount.x * _direction.InvertBinary().x;
        startY -= _movCount.y * _direction.InvertBinary().y;
        startZ -= _movCount.z * _direction.InvertBinary().z;

        //Save current position for next frame
        _oldPos = FocusPoint;

        CreateLineMaterial();
        // set the current material
        lineMaterial.SetPass(0);

        GL.Begin(GL.LINES);
       
        if (showSub)
        {
            GL.Color(subColor);

            //Layers
            for (float j = 0; j <= gridSizeY; j += smallStep)
            {
                //X axis lines
                for (float i = 0; i <= gridSizeZ; i += smallStep)
                {
                    GL.Vertex3(startX, startY + j, startZ + i);
                    GL.Vertex3(startX + gridSizeX, startY + j, startZ + i);
                }

                //Z axis lines
                for (float i = 0; i <= gridSizeX; i += smallStep)
                {
                    GL.Vertex3(startX + i, startY + j, startZ);
                    GL.Vertex3(startX + i, startY + j, startZ + gridSizeZ);
                }
            }

            //Y axis lines
            for (float i = 0; i <= gridSizeZ; i += smallStep)
            {
                for (float k = 0; k <= gridSizeX; k += smallStep)
                {
                    GL.Vertex3(startX + k, startY, startZ + i);
                    GL.Vertex3(startX + k, startY + gridSizeY, startZ + i);
                }
            }
        }

        if (showMain)
        {
            GL.Color(mainColor);

            //Layers
            for (float j = 0; j <= gridSizeY; j += largeStep)
            {
                //X axis lines
                for (float i = 0; i <= gridSizeZ; i += largeStep)
                {
                    GL.Vertex3(startX, startY + j, startZ + i);
                    GL.Vertex3(startX + gridSizeX, startY + j, startZ + i);
                }

                //Z axis lines
                for (float i = 0; i <= gridSizeX; i += largeStep)
                {
                    GL.Vertex3(startX + i, startY + j, startZ);
                    GL.Vertex3(startX + i, startY + j, startZ + gridSizeZ);
                }
            }

            //Y axis lines
            for (float i = 0; i <= gridSizeZ; i += largeStep)
            {
                for (float k = 0; k <= gridSizeX; k += largeStep)
                {
                    GL.Vertex3(startX + k, startY, startZ + i);
                    GL.Vertex3(startX + k, startY + gridSizeY, startZ + i);
                }
            }
        }
        GL.End();
    }
    /*
    void OnPostRender()
    {
        if (!_isEnabled) return;
        if (smallStep < 0.1f || largeStep < 0.1f) return;

        FocusPoint = _targetObj.transform.position;
        startX = FocusPoint.x;
        startY = FocusPoint.y;
        startZ = FocusPoint.z;

        CreateLineMaterial();
        // set the current material
        lineMaterial.SetPass(0);

        GL.Begin(GL.LINES);


        if (showSub)
        {
            GL.Color(subColor);

            //Layers
            for (float j = 0; j <= gridSizeY; j += smallStep)
            {
                //X axis lines
                for (float i = 0; i <= gridSizeZ; i += smallStep)
                {
                    GL.Vertex3(startX, startY + j, startZ + i);
                    GL.Vertex3(startX + gridSizeX, startY + j, startZ + i);
                }

                //Z axis lines
                for (float i = 0; i <= gridSizeX; i += smallStep)
                {
                    GL.Vertex3(startX + i, startY + j, startZ);
                    GL.Vertex3(startX + i, startY + j, startZ + gridSizeZ);
                }
            }

            //Y axis lines
            for (float i = 0; i <= gridSizeZ; i += smallStep)
            {
                for (float k = 0; k <= gridSizeX; k += smallStep)
                {
                    GL.Vertex3(startX + k, startY, startZ + i);
                    GL.Vertex3(startX + k, startY + gridSizeY, startZ + i);
                }
            }
        }

        if (showMain)
        {
            GL.Color(mainColor);

            //Layers
            for (float j = 0; j <= gridSizeY; j += largeStep)
            {
                //X axis lines
                for (float i = 0; i <= gridSizeZ; i += largeStep)
                {
                    GL.Vertex3(startX, startY + j, startZ + i);
                    GL.Vertex3(startX + gridSizeX, startY + j, startZ + i);
                }

                //Z axis lines
                for (float i = 0; i <= gridSizeX; i += largeStep)
                {
                    GL.Vertex3(startX + i, startY + j, startZ);
                    GL.Vertex3(startX + i, startY + j, startZ + gridSizeZ);
                }
            }

            //Y axis lines
            for (float i = 0; i <= gridSizeZ; i += largeStep)
            {
                for (float k = 0; k <= gridSizeX; k += largeStep)
                {
                    GL.Vertex3(startX + k, startY, startZ + i);
                    GL.Vertex3(startX + k, startY + gridSizeY, startZ + i);
                }
            }
        }


        GL.End();
    }
    */
}