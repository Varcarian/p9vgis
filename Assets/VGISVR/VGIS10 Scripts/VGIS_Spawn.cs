﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using VGIS;

public class VGIS_Spawn : MonoBehaviour
{
    private Vector3 _pos;
    private Quaternion _rot;
    private VGIS_InteractableItem _item;
	// Use this for initialization
	void Start ()
	{
	    _pos = transform.position;
	    _rot = transform.rotation;
	    _item = GetComponent<VGIS_InteractableItem>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (_item.IsAttached)
        {
            VGIS_Interaction hand = _item.AttachedHand;
            _item.ForceDetach();
            GameObject news = Instantiate(gameObject);

            Destroy(news.GetComponent<VGIS_Spawn>());
	        news.GetComponent<BoxCollider>().isTrigger = false;
	        news.GetComponent<Rigidbody>().isKinematic = true;
            hand.BeginInteraction(news.GetComponent<VGIS_InteractableItem>());

            _item.transform.position = _pos;
            _item.transform.rotation = _rot;
        }                  
    }
}
