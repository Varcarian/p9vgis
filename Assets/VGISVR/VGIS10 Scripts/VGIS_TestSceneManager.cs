﻿#define IS_FINAL_TEST
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VGIS;
using VRTK;

public class VGIS_TestSceneManager : MonoBehaviour
{
    public static VGIS_TestSceneManager Instance;
    public static int FrustrationMeter;
    private string _scene0 = "PracticeTest", _scene1 = "ProximityItemTest", _scene2 = "ContItemTest", _scene3 = "PreviewItemTest";
    private string _displayString;
    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start () {
        _displayString = _scene0;
    }
	
	// Update is called once per frame
	void Update ()
    {
#if IS_FINAL_TEST
        if (Input.GetKey(KeyCode.RightShift) && Input.GetKey(KeyCode.P))
        {
            SceneManager.LoadScene(_scene0);
            _displayString = _scene0;
            FrustrationMeter = 0;
        }
        if (Input.GetKey(KeyCode.RightShift) && Input.GetKey(KeyCode.A))
        {
            SceneManager.LoadScene(_scene1);
            _displayString = _scene1;
            FrustrationMeter = 0;
        }
        if (Input.GetKey(KeyCode.RightShift) && Input.GetKey(KeyCode.B))
        {
            SceneManager.LoadScene(_scene2);
            _displayString = _scene2;
            FrustrationMeter = 0;
        }
        if (Input.GetKey(KeyCode.RightShift) && Input.GetKey(KeyCode.C))
        {
            SceneManager.LoadScene(_scene3);
            _displayString = _scene3;
            FrustrationMeter = 0;
        }
#endif
    }

    private void OnGUI()
    {
        #if IS_FINAL_TEST
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h / 10;
        style.normal.textColor = new Color(1.0f, 0.0f, 1.0f, 1.0f);
        GUI.Label(rect, _displayString + " : " + FrustrationMeter, style);
        #endif
    }
 
}
