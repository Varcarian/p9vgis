﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting;
using UnityEngine;
using VGIS;

public class VGIS_FaceIndicator : MonoBehaviour
{
    public VGIS_FaceIndicator Instance;
    public Material SurfaceMaterial;
    private RaycastHit _leftHit;
    private RaycastHit _rightHit;

    private GameObject _lastLeftObj, _lastRightObj;
    private Vector3 _lastLeftNormal, _lastRightNormal;

    private Dictionary<GameObject, Dictionary<Vector3, GameObject>> _hightlightObjects = new Dictionary<GameObject, Dictionary<Vector3, GameObject>>();

    // Use this for initialization
    void Start()
    {
        Instance = this;
        GameObject cube = GameObject.Find("Cube");
        //PlaceIndicators(cube, new[] { new Vector3(1, 0, 0), new Vector3(0, 1, 0), new Vector3(0, 0, 1) }, true);
    }

    // Update is called once per frame
    void Update()
    {
        _leftHit = VGIS_Player.Instance.LeftHand.Hit;
        _rightHit = VGIS_Player.Instance.RightHand.Hit;

        if (_leftHit.transform != null)
        {
            PlaceIndicator(_leftHit.transform.gameObject, _leftHit.normal, false);
            _lastLeftNormal = _leftHit.normal;
            _lastLeftObj = _leftHit.transform.gameObject;
        }
        if (_rightHit.transform != null)
        {
            PlaceIndicator(_rightHit.transform.gameObject, _rightHit.normal, false);
            _lastRightNormal = _rightHit.normal;
            _lastLeftObj = _rightHit.transform.gameObject;
        }
        if(_leftHit.transform == null)
        {
            RemoveIndicator(_lastLeftObj, _lastLeftNormal, false);
            _lastLeftNormal = new Vector3();
            _lastLeftObj = null;
        }
        if(_rightHit.transform == null)
        {
            RemoveIndicator(_lastRightObj, _lastRightNormal, false);
            _lastRightNormal = new Vector3();
            _lastLeftObj = null;
        }    
    }

    public void PlaceIndicator(GameObject obj, Vector3 normal, bool isLocalSpace)
    {
        if (!_hightlightObjects.ContainsKey(obj))
            _hightlightObjects.Add(obj, new Dictionary<Vector3, GameObject>()); // Create dictionary entry for object if it does not exist

        Vector3 extents = obj.GetComponent<MeshFilter>().mesh.bounds.extents; //The extents of the object
        float scaleFactor = 1.001f; //Used to avoid z fighting

        var localNormal = isLocalSpace ? normal : obj.transform.InverseTransformDirection(normal);

        if (_hightlightObjects[obj].ContainsKey(localNormal)) return;
        GameObject indicator = SetupMeshObject(localNormal); //Create new indicator           
        indicator.transform.SetParent(obj.transform); //Set the parent of the new indicator to the object
        indicator.transform.localRotation = Quaternion.LookRotation(localNormal, localNormal == obj.transform.up ? obj.transform.right : obj.transform.up); //Rotate the indicator correctly
        indicator.transform.localPosition = new Vector3(extents.x * localNormal.x * scaleFactor,
            extents.y * localNormal.y * scaleFactor,
            extents.z * localNormal.z * scaleFactor); //Set the position to the extent of the object in the correct direction
        indicator.transform.localScale = new Vector3(1.1f, 1.1f, 1); // Scale the indicator to be slightly bigger than the surface of the object
        _hightlightObjects[obj].Add(localNormal, indicator); //Add indicator to dictionary
    }
    private void PlaceIndicators(GameObject obj, Vector3[] normals, bool isLocalSpace)
    {
        if(!_hightlightObjects.ContainsKey(obj))
            _hightlightObjects.Add(obj,new Dictionary<Vector3, GameObject>()); // Create dictionary entry for object if it does not exist

        Vector3 extents = obj.GetComponent<MeshFilter>().mesh.bounds.extents; //The extents of the object
        float scaleFactor = 1.001f; //Used to avoid z fighting
        foreach (Vector3 normal in normals)
        {
            var localNormal = isLocalSpace ? normal : obj.transform.InverseTransformDirection(normal);
            
            if (_hightlightObjects[obj].ContainsKey(localNormal)) continue; //If an indicator for the current normal already exists than skip this loop iteration
            
            GameObject indicator = SetupMeshObject(localNormal); //Create new indicator           
            indicator.transform.SetParent(obj.transform); //Set the parent of the new indicator to the object
            indicator.transform.localRotation = Quaternion.LookRotation(localNormal, localNormal == Vector3.up ? Vector3.right : Vector3.up); //Rotate the indicator correctly
            indicator.transform.localPosition = new Vector3(extents.x * localNormal.x * scaleFactor,
                                                            extents.y * localNormal.y * scaleFactor, 
                                                            extents.z * localNormal.z * scaleFactor); //Set the position to the extent of the object in the correct direction
            indicator.transform.localScale = new Vector3(1.1f,1.1f,1); // Scale the indicator to be slightly bigger than the surface of the object
            _hightlightObjects[obj].Add(localNormal, indicator); //Add indicator to dictionary
        }
    }


    public void RemoveIndicator(GameObject obj, Vector3 normal, bool isLocalSpace)
    {       
        if (obj == null) return;
        if (!_hightlightObjects.ContainsKey(obj)) return;
        var localNormal = isLocalSpace ? normal : obj.transform.InverseTransformDirection(normal);
        if (!_hightlightObjects[obj].ContainsKey(localNormal)) return;
        Destroy(_hightlightObjects[obj][localNormal]);
        _hightlightObjects[obj].Remove(localNormal);
    }

    public void RemoveIndicators(GameObject obj, bool isLocalSpace, Vector3[] normals = default(Vector3[]))
    {
        if (obj == null) return;
        if (!_hightlightObjects.ContainsKey(obj)) return;
        if (normals == null)
        {           
            foreach (Vector3 normal in _hightlightObjects[obj].Keys)
            {
                Destroy(_hightlightObjects[obj][normal]);                
            }
            _hightlightObjects.Remove(obj);
        }
        else
        {
            foreach (Vector3 normal in normals)
            {
                var localNormal = isLocalSpace ? normal : obj.transform.InverseTransformDirection(normal);
                if (!_hightlightObjects[obj].ContainsKey(localNormal)) return;
                Destroy(_hightlightObjects[obj][localNormal]);
                _hightlightObjects[obj].Remove(localNormal);
            }
            if (_hightlightObjects[obj].Count == 0) _hightlightObjects.Remove(obj);
        }
    }

    private GameObject SetupMeshObject(Vector3 normal = default(Vector3))
    {
        var _gO = new GameObject("Indicator " + normal);
        MeshFilter mf = _gO.AddComponent<MeshFilter>();
        _gO.AddComponent<MeshRenderer>();
        Mesh mesh = mf.mesh;

        Vector3[] vertices = new Vector3[]
        {
            new Vector3(-0.5f, -0.5f, 0), //top left: 0
            new Vector3(0.5f,-0.5f,0), //top right : 1
            new Vector3(-0.5f,0.5f,0), //bottom left : 2
            new Vector3(0.5f,0.5f) //bottom right : 3
        };

        int[] triangles = new int[]
        {
            0,1,2, //left tri
            1,3,2 //right tri
        };

        Vector2[] uvs = new Vector2[]
        {
            new Vector2(0,1),
            new Vector2(0,0),
            new Vector2(1,1),
            new Vector2(1,0)  
        };

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals();
        _gO.GetComponent<MeshRenderer>().material = SurfaceMaterial;
        return _gO;
    }
}
