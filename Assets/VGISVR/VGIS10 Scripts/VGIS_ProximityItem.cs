﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VGIS;

[RequireComponent(typeof(BoxCollider))]
public class VGIS_ProximityItem : VGIS_InteractableItem
{

    private static readonly Vector3[] AllDirections = new[] { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };
    private static readonly Vector3[] XZDirections = new[] { Vector3.left, Vector3.right, Vector3.forward, Vector3.back };
    public static int FloorHeight = 0;
    private ProxyRayHits _hits = new ProxyRayHits();
    private float _rayCastDistance = 2;
    private GameObject _ghostObject;
    private bool _ghostEnabled = false;
    private bool _canPlace = false;

    public ItemType ProxyItemType = ItemType.Floor;

    public enum ItemType
    {
        Floor,
        Wall,
        Ceiling
    }


    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _hits = new ProxyRayHits();
        Rigidbody.freezeRotation = true;
        Rigidbody.isKinematic = true;
        DisableKinematicOnAttach = true;
        EnableKinematicOnDetach = true;
        SetupGhost();
        AdjustCollider();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    public override void OnNewPosesApplied()
    {
        Quaternion rotation = transform.rotation;
        base.OnNewPosesApplied();
        transform.rotation = rotation;
    }

    IEnumerator ShowGhost()
    {
        while (_ghostEnabled)
        {
            _ghostObject.transform.localScale = this.transform.localScale * 0.999f;
            DoRaycasts();
            if (_hits.Count > 0)
            {
                _ghostObject.SetActive(true);

                Vector3 ext = WorldExtents;

                switch (ProxyItemType)
                {
                    case ItemType.Floor:
                        ProxyRayHit closestHit = _hits.GetShortestRay(XZDirections);
                        if (closestHit == null) // Check if something was hit
                        {
                            _ghostObject.SetActive(false);
                            _ghostObject.transform.position = transform.position;
                            _ghostObject.transform.rotation = transform.rotation;
                            break;
                        }
                        RaycastHit hit = closestHit.ShortHit;                      
                                                             
                        if (!hit.transform.GetComponent<VGIS_ProximityItem>()) // If we are not hitting an object
                        {
                            Vector3 pos = hit.point + new Vector3(ext.z * hit.normal.x, 0, ext.z * hit.normal.z);//Use Z in both cases since we the object will always be facing in its local z direction
                            pos.y = FloorHeight+ext.y; // Move to floor height
                            _ghostObject.transform.position = pos;  
                            _ghostObject.transform.rotation = Quaternion.LookRotation(hit.normal, Vector3.up);
                        }
                        else
                        {
                            if (hit.transform.forward == hit.normal) // Check if we hit the local forward of an object
                            {
                                Vector3 closeExts = hit.transform.GetComponent<VGIS_InteractableItem>().WorldExtents;
                                Vector3 direction = new Vector3();
                                if (hit.transform.right.Abs() == Vector3.right) // if the transforms side axis is x
                                {
                                    if (hit.transform.position.x > hit.point.x) // Check which side to place the object on
                                        direction = hit.transform.right * -1;
                                    else
                                        direction = hit.transform.right;
                                }
                                else if (hit.transform.right.Abs() == Vector3.forward) // if the transforms side axis is z
                                {
                                    ext = new Vector3(ext.z, ext.y, ext.x); //swap the bounds to properly account for changed axis
                                    closeExts = new Vector3(closeExts.z, closeExts.y, closeExts.x);
                                    if (hit.transform.position.z > hit.point.z) // Check which side to place the object on
                                        direction = hit.transform.forward * -1;
                                    else
                                        direction = hit.transform.forward;
                                }
                                _ghostObject.transform.position = hit.transform.position + hit.transform.InverseTransformDirection(direction).Mult(closeExts + ext) + // Move to the correct side
                                                                  hit.transform.up.Mult(closeExts * -1 + ext) + // Move to floor
                                                                  hit.transform.forward.Mult(closeExts * -1 + ext); // Move to wall

                                _ghostObject.transform.rotation = Quaternion.LookRotation(hit.normal, Vector3.up);
                            }
                            else if(hit.transform.forward*-1 == hit.normal) // We hit the back of the object
                            {
                                Vector3 pos =  hit.point + new Vector3(ext.z * hit.normal.x, 1, ext.z * hit.normal.z);
                                pos.y = FloorHeight+ext.y;
                                _ghostObject.transform.position = pos;
                                _ghostObject.transform.rotation = Quaternion.LookRotation(Math.Abs(Mathf.Abs(hit.normal.y) - transform.up.y) < 0.001f ? transform.forward : hit.normal, Vector3.up);
                            }
                            else if (hit.transform.up == hit.normal) // We hit the top of an object
                            {
                                Vector3 closeExts = hit.transform.GetComponent<VGIS_InteractableItem>().Bounds.extents;
                                _ghostObject.transform.position = hit.transform.position + hit.transform.InverseTransformDirection(hit.transform.up).Mult(closeExts + ext) + // Move to top and center
                                                                  hit.transform.forward.Mult(closeExts * -1 + ext); // Move to wall

                                _ghostObject.transform.rotation = Quaternion.LookRotation(hit.transform.forward, Vector3.up);
                            }
                            else // Else we hit the side of an object (Same code as for hitting forwards)
                            {
                                Vector3 closeExts = hit.transform.GetComponent<VGIS_InteractableItem>().WorldExtents;
                                Vector3 direction = new Vector3();
                                if (hit.transform.right.Abs() == Vector3.right) // if the transforms side axis is x
                                {
                                    if (hit.transform.position.x > hit.point.x) // Check which side to place the object on
                                        direction = hit.transform.right * -1;
                                    else
                                        direction = hit.transform.right;
                                }
                                else if (hit.transform.right.Abs() == Vector3.forward) // if the transforms side axis is z
                                {
                                    ext = new Vector3(ext.z, ext.y, ext.x); //swap the bounds to properly account for changed axis
                                    closeExts = new Vector3(closeExts.z, closeExts.y, closeExts.x);
                                    if (hit.transform.position.z > hit.point.z) // Check which side to place the object on
                                        direction = hit.transform.forward * -1;
                                    else
                                        direction = hit.transform.forward;
                                }
                                _ghostObject.transform.position = hit.transform.position + hit.transform.InverseTransformDirection(direction).Mult(closeExts + ext) + // Move to the correct side
                                                                  hit.transform.up.Mult(closeExts * -1 + ext) + // Move to floor
                                                                  hit.transform.forward.Mult(closeExts * -1 + ext); // Move to wall

                                _ghostObject.transform.rotation = hit.transform.rotation;
                            }
                        }
                        break;
                    case ItemType.Wall:
                        break;
                    case ItemType.Ceiling:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }                
                int layerMask = 1 << LayerMask.NameToLayer("CurrentBox");
                layerMask = ~layerMask;
                Collider[] colliders = Physics.OverlapBox(_ghostObject.transform.position, Bounds.extents * 0.999f, _ghostObject.transform.rotation, layerMask);
                if (colliders.Length > 0)
                {
                    foreach (MeshRenderer child in _ghostObject.GetComponentsInChildren<MeshRenderer>())
                    {                       
                        foreach (Material material in child.materials)
                        {
                            material.color = new Color(1, 0, 0, 0.5f);
                        }
                    }
                    _ghostObject.transform.localScale = this.transform.localScale * 1.001f;
                    _canPlace = false;
                }
                else
                {
                    foreach (MeshRenderer child in _ghostObject.GetComponentsInChildren<MeshRenderer>())
                    {
                        foreach (Material material in child.materials)
                        {
                            material.color = new Color(0, 1, 0, 0.5f);
                        }
                    }
                    _ghostObject.transform.localScale = this.transform.localScale * 0.999f;
                    _canPlace = true;
                }
            }
            else
            {
                _canPlace = false;
                _ghostObject.SetActive(false);
            }
            yield return new WaitForFixedUpdate();
        }
        _ghostObject.SetActive(false);
    }

    void DoRaycasts()
    {
        Vector3 ext = GetComponent<Collider>().bounds.extents;
        List<ProxyRayHit> hits = new List<ProxyRayHit>();    

        foreach (Vector3 direction in AllDirections)
        {
            int layerMask1 = 1 << LayerMask.NameToLayer("Interactable");
            int layerMask2 = 1 << LayerMask.NameToLayer("Room");
            RaycastHit rHit;
            RaycastHit bHit;
            ProxyRayHit pHit = new ProxyRayHit(direction);
            if (Physics.Raycast(transform.position + ext.Mult(direction)*0.999f, direction, out rHit, _rayCastDistance, layerMask2))
            {
                pHit.SetRayHit(rHit);
            }

            if (Physics.BoxCast(transform.position-direction*0.001f, ext, direction , out bHit, transform.rotation, _rayCastDistance, layerMask1))
            {
                pHit.SetBoxHit(bHit);
            }  
            if(rHit.transform != null || bHit.transform != null)
                hits.Add(pHit);         
        }
        _hits = new ProxyRayHits(hits);
    }

    void SetupGhost()
    {
        _ghostObject = new GameObject(name + " : Ghost");
        _ghostObject.SetActive(false);

        MeshRenderer[] childs = GetComponentsInChildren<MeshRenderer>();

        foreach (var child in childs)
        {
            GameObject newChild = new GameObject(child.gameObject.name + " : GhostPart");
            newChild.transform.SetParent(_ghostObject.transform);
            newChild.AddComponent<MeshFilter>().mesh = child.transform.GetComponent<MeshFilter>().mesh;
            newChild.AddComponent<MeshRenderer>().materials = child.materials;

            foreach (Material mat in newChild.GetComponent<MeshRenderer>().materials)
            {
                mat.SetFloat("_Mode", 3.0f);
                mat.shader = Shader.Find("Legacy Shaders/Transparent/VertexLit");
                mat.color = new Color(0, 1, 0, 0.5f);
                //newChild.transform.localPosition = child.transform.localPosition;
                newChild.transform.localPosition = child.name == transform.name ? Vector3.zero : child.transform.localPosition; //TODO: In case of bug check this ( name is the same in the game it's a shame)
                newChild.transform.localRotation = child.transform.localRotation;
            }           
        }
        _ghostObject.transform.localScale = transform.localScale * 0.999f;
    }

    public override void BeginInteraction(VGIS_Interaction hand)
    {
        base.BeginInteraction(hand);
        gameObject.layer = LayerMask.NameToLayer("CurrentBox");
        _ghostEnabled = true;
        StartCoroutine(ShowGhost());

        if(!IsSelected)
            Hightlight(hand);
    }

    public override void EndInteraction()
    {
        if(!IsSelected)
            UnHightlight(AttachedHand);

        base.EndInteraction();
        gameObject.layer = LayerMask.NameToLayer("Interactable");
        if (_canPlace)
        {
            transform.position = _ghostObject.transform.position;
            transform.rotation = _ghostObject.transform.rotation;
        }
        _ghostEnabled = false;
        StopCoroutine(ShowGhost());      
    }

    public class ProxyRayHit
    {
        public Vector3 Direction { get; private set; }
        public RaycastHit RayHit { get; private set; }
        public RaycastHit BoxHit { get; private set; }
        public RaycastHit ShortHit { get; private set; }

        public ProxyRayHit(Vector3 dir)
        {
            Direction = dir;
        }

        public void SetRayHit(RaycastHit hit)
        {
            RayHit = hit;
            if (BoxHit.transform == null)
            {
                ShortHit = RayHit;
            }
            else
            {
                ShortHit = RayHit.distance < BoxHit.distance ? RayHit : BoxHit;
            }
        }

        public void SetBoxHit(RaycastHit hit)
        {
            BoxHit = hit;
            if (RayHit.transform == null)
            {
                ShortHit = BoxHit;
            }
            else
            {
                ShortHit = RayHit.distance < BoxHit.distance ? RayHit : BoxHit;
            }
        }

        public override string ToString()
        {

            string ray, box;
            if (RayHit.transform == null)
                ray = " Ray: null";
            else
            {
                ray = "RayHit: " + RayHit.transform.name + " Distance: " + RayHit.distance;
            }
            if (BoxHit.transform == null)
                box = " Box: null";
            else
            {
                box = " BoxHit: " + BoxHit.transform.name + " Distance: " + BoxHit.distance;
            }

            return ray + box;
        }
    }

    private class ProxyRayHits
    {
        public int Count
        {           
            get { return Hits.Count; }
        }

        public ProxyRayHits()
        {
            Hits = new List<ProxyRayHit>();
        }
        public ProxyRayHits(List<ProxyRayHit> hits)
        {
            Hits = hits;
        }

        public List<ProxyRayHit> Hits { get; set; }

        public ProxyRayHit GetShortestRay(Vector3[] directions)
        {

            var requestedHits = (from firstItem in directions
                join secondItem in Hits
                on firstItem equals secondItem.Direction
                select secondItem).ToList();

            return requestedHits.OrderBy(x => x.ShortHit.distance).FirstOrDefault();
        }
    }
}
