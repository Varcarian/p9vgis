﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VGIS;

public class VGIS_TwoHandItem : VGIS_InteractableItem
{
    public static VGIS_TwoHandItem LastInteracted;

    public bool IsFineMove = false;

    private Vector3 _itemStartPoint;
    private Vector3 _handStartPoint;
    private VGIS_Interaction _hand;

    protected override void Awake()
    {
        base.Awake();
        DisableKinematicOnAttach = true;
        EnableKinematicOnDetach = true;
        Rigidbody.isKinematic = true;
        Rigidbody.freezeRotation = true;
    }

    protected override void Start()
    {
        AdjustCollider();
    }

    public override void BeginInteraction(VGIS_Interaction hand)
    {
        base.BeginInteraction(hand);
        LastInteracted = this;

    }

    public void BeginFineMovement(VGIS_Interaction hand)
    {
        _hand = hand;
        _handStartPoint = _hand.transform.position;
        _itemStartPoint = transform.position;
        gameObject.layer = LayerMask.NameToLayer("CurrentBox");
        IsFineMove = true;
    }

    public void EndFineMovement()
    {
        IsFineMove = false;
        _hand = null;
        gameObject.layer = LayerMask.NameToLayer("Interactable");
    }

    protected override void Update()
    {
        base.Update();
        if (!IsFineMove) return;
        if (IsAttached) return;
        Vector3 movVector = (_hand.transform.position - _handStartPoint) *2;
        Vector3 target = _itemStartPoint + movVector;


        int layerMask = 1 << LayerMask.NameToLayer("CurrentBox");
        layerMask = ~layerMask;
        if (!Physics.CheckBox(target, WorldExtents * 0.99f, transform.rotation, layerMask))
        {
           
            transform.position = target;
        }
        else
        {
            _itemStartPoint = transform.position;
            _handStartPoint = _hand.transform.position;
        }

    }
}
