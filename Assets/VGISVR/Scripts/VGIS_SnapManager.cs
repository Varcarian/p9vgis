﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using VGIS;

public enum GhostStates
{
    GhostSnap,
    GhostNoSnap,
    GhostEmptyAssembly
}

public class VGIS_SnapManager : MonoBehaviour
{
    public VGIS_InteractableItem Item;
    private readonly Dictionary<GameObject, bool> _parts = new Dictionary<GameObject, bool>(); //todo: make this vgis_snapable?
    public readonly Dictionary<GameObject, Vector3> OriginalPositions = new Dictionary<GameObject, Vector3>();
    //public readonly Dictionary<GameObject, Quaternion> OriginalRoations = new Dictionary<GameObject, Quaternion>(); //todo: Fix rotations later?
    public int PartAmount;
    public List<Renderer> ActiveGhosts = new List<Renderer>();
    public Dictionary<GameObject, Renderer> GhostRenderers = new Dictionary<GameObject, Renderer>();
    private Material _ghostMaterialSnap;
    private Material _ghostMaterialNoSnap;
    private Material _ghostMaterialAssembly;
    private readonly List<GameObject> _listOfChildren = new List<GameObject>();
    public bool IsEmpty { get; private set; }

    public VGIS_SnapManager()
    {
        IsEmpty = false;       
    }

    public Dictionary<GameObject, bool> Parts
    {
        get { return _parts; }
    }

    void Awake()
    {
        _ghostMaterialSnap = Instantiate((Material)Resources.Load("GhostMaterialSnap"));
        _ghostMaterialNoSnap = Instantiate((Material)Resources.Load("GhostMaterialNoSnap"));
        _ghostMaterialAssembly = Instantiate((Material)Resources.Load("GhostMaterialAssembly"));
        Shader shader = Shader.Find("VGIS_VR/StandardTransparent");
        GetListOfMeshChildren(this.gameObject);
        Vector3 boundsAverage = new Vector3();

        foreach (GameObject child in _listOfChildren)
        {
            child.transform.SetParent(null);
            boundsAverage += child.GetComponent<MeshRenderer>().bounds.center;
        }

        boundsAverage = boundsAverage/_listOfChildren.Count;
        gameObject.transform.position = boundsAverage;

        foreach (GameObject child in _listOfChildren)
        {
            child.transform.SetParent(gameObject.transform);
        }

        //Make ghosts and store positions in localspace
        foreach (GameObject child in _listOfChildren)
        {
            //todo: clean this shit up?
            GameObject parent = new GameObject(child.name + "_Parent");
            GameObject ghostParent = new GameObject(child.name + "_GhostParent");
            GameObject ghostModel = new GameObject(child.name + "_GhostModel");

            parent.transform.position = child.GetComponent<Renderer>().bounds.center;
            ghostParent.transform.position = parent.transform.position;
            ghostModel.transform.position = child.transform.position;           
                
            parent.transform.SetParent(this.transform);
            ghostParent.transform.SetParent(this.transform);

            parent.transform.localEulerAngles = Vector3.zero;
            ghostParent.transform.localEulerAngles = Vector3.zero;
         
            parent.AddComponent<BoxCollider>().size = child.GetComponent<Renderer>().bounds.size;
            ghostParent.AddComponent<BoxCollider>().size = child.GetComponent<Renderer>().bounds.size;
            ghostParent.GetComponent<Collider>().enabled = false;

            parent.AddComponent<VGIS_InteractableItemSnapable>().Assembly = this;           
            child.GetComponent<Renderer>().sharedMaterial.shader = shader;

            ghostModel.transform.localScale = child.transform.localScale;
            ghostModel.AddComponent<MeshFilter>();
            ghostModel.GetComponent<MeshFilter>().mesh = child.GetComponent<MeshFilter>().mesh;
            
            ghostModel.AddComponent<MeshRenderer>();

            ghostModel.GetComponent<MeshRenderer>().materials = SetAllMaterials(child.GetComponent<Renderer>(), _ghostMaterialSnap);

            child.transform.SetParent(parent.transform);
            
            ghostModel.transform.SetParent(ghostParent.transform);
            ghostModel.transform.localEulerAngles = Vector3.zero;

            _parts.Add(parent.gameObject, true);
            GhostRenderers.Add(parent.gameObject, ghostModel.GetComponent<Renderer>());
            ghostModel.SetActive(false);

            OriginalPositions.Add(parent, parent.transform.localPosition);

            PartAmount++;
        }
        
        this.gameObject.AddComponent<Rigidbody>();
        Item = this.gameObject.AddComponent<VGIS_InteractableItem>();
        
    //ApplyLowPolyMeshColliders();
}

    private Material[] SetAllMaterials(Renderer render, Material mat)
    {
        List<Material> temp = render.materials.ToList();

        for (int i = 0; i < temp.Count; i++)
        {
            temp[i] = mat;
        }
        return temp.ToArray();
    }

    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
    //todo: make this more robust, check names?
    [Obsolete]
    private void ApplyLowPolyMeshColliders()
    {
        GameObject reducedPoly = Resources.Load<GameObject>("ReducedPoly/" + this.name + "Reduced");
        List<MeshCollider> colliders = new List<MeshCollider>();
        foreach (Transform mesh in reducedPoly.transform)
        {
            Debug.Log(mesh);
            _parts.Keys.ElementAt(mesh.GetSiblingIndex()).AddComponent<MeshCollider>();
            _parts.Keys.ElementAt(mesh.GetSiblingIndex()).GetComponent<MeshCollider>().sharedMesh = mesh.GetComponent<MeshFilter>().sharedMesh;
            _parts.Keys.ElementAt(mesh.GetSiblingIndex()).GetComponent<MeshCollider>().convex = true;
        }

    }
    
    private void ShowAllGhostsWithColliders()
    {
        foreach (Renderer ghost in GhostRenderers.Values)
        {
            if (ActiveGhosts.Contains(ghost)) continue;
            ActiveGhosts.Add(ghost);
            ghost.gameObject.SetActive(true);
            ghost.transform.parent.GetComponent<Collider>().enabled = true;
        }
    }

    private void DisableAllGhosts(GameObject obj)
    {
        foreach (Renderer ghost in GhostRenderers.Values)
        {
            if (ghost.transform.parent == obj) continue;
            if (!ActiveGhosts.Contains(ghost)) continue;
            ghost.gameObject.SetActive(false);
            ghost.transform.parent.GetComponent<Collider>().enabled = false;
            ActiveGhosts.Remove(ghost);
        }
    }

    public void ShowGhost(VGIS_InteractableItemSnapable ghostSource)
    {
        if (ActiveGhosts.Contains(GhostRenderers[ghostSource.gameObject])) return;
        GhostRenderers[ghostSource.gameObject].gameObject.SetActive(true);
        ActiveGhosts.Add(GhostRenderers[ghostSource.gameObject]);
    }

    public void DisableGhost(VGIS_InteractableItemSnapable ghostSource)
    {
        if (ActiveGhosts.Count <= _parts.Count-1)
        {
            if (!ActiveGhosts.Contains(GhostRenderers[ghostSource.gameObject])) return;
            GhostRenderers[ghostSource.gameObject].gameObject.SetActive(false);
            ActiveGhosts.Remove(GhostRenderers[ghostSource.gameObject]);
        }
        else
        {
            SetGhostState(ghostSource, GhostStates.GhostEmptyAssembly);
        }
    }

    public float GetDistanceToGhost(VGIS_InteractableItemSnapable part)
    {
        return Vector3.Distance(GhostRenderers[part.gameObject].transform.GetComponent<Renderer>().bounds.center, part.Renderer.bounds.center);
    }

    public void SetGhostState(VGIS_InteractableItemSnapable ghostSource, GhostStates newState)
    {
        switch (newState)
        {
            case GhostStates.GhostSnap:
                GhostRenderers[ghostSource.gameObject].materials = SetAllMaterials(GhostRenderers[ghostSource.gameObject], _ghostMaterialSnap);
                break;
            case GhostStates.GhostNoSnap:
                GhostRenderers[ghostSource.gameObject].materials = SetAllMaterials(GhostRenderers[ghostSource.gameObject], _ghostMaterialNoSnap);
                break;
            case GhostStates.GhostEmptyAssembly:
                GhostRenderers[ghostSource.gameObject].materials = SetAllMaterials(GhostRenderers[ghostSource.gameObject], _ghostMaterialAssembly);
                break;
            default:
                throw new ArgumentOutOfRangeException("newState", newState, null);
        }
    }

    private void SetAllGhostsEmpty()
    {
        foreach (Renderer ghostRenderer in GhostRenderers.Values)
        {
            ghostRenderer.materials = SetAllMaterials(ghostRenderer, _ghostMaterialAssembly);
        }
    }
    public void SetAttachedState(VGIS_InteractableItemSnapable item, bool state)
    {
        int lastAmount = PartAmount;

        if (!_parts.ContainsKey(item.gameObject)) return;

        _parts[item.gameObject] = state;
        switch (state)
        {
            case true:
                PartAmount++;                
                break;
            case false:
                PartAmount--;
                break;
            default:
                break;
        }
        if (PartAmount == 0)
        {
            IsEmpty = true;
            ShowAllGhostsWithColliders();
            SetAllGhostsEmpty();
        }
        else if (lastAmount == 0)
        {
            IsEmpty = false;
            DisableAllGhosts(item.gameObject);
        }
    }

    private void GetListOfMeshChildren(GameObject obj)
    {       
        if(null == obj)
            return;

        foreach (Transform child in obj.transform)
        {
            if(null == child) continue;
            if (child.GetComponent<MeshRenderer>())
            {
                _listOfChildren.Add(child.gameObject);
            }           
            GetListOfMeshChildren(child.gameObject);           
        }
    }    
}


