﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using VRTK.Highlighters;

namespace VGIS
{
    public abstract class VGIS_Interactable : MonoBehaviour
    {
        public Rigidbody Rigidbody;

        public bool CanAttach = true;
        public bool CanSelect = true;
        public bool CanHoverHightlight = true;
        public bool IsSelected = false;
        public bool IsHightlighted = false;
        public bool IsHoverHightlighted = false;
        public bool DisableKinematicOnAttach = true;
        public bool EnableKinematicOnDetach = false;
        public float DropDistance = 1;
        protected List<MeshRenderer> _listOfRenderers = new List<MeshRenderer>();
        protected List<Material> _listOfHoverMaterials = new List<Material>();
        public bool EnableGravityOnDetach = true;
        public IEnumerator HighligtherEnumerator;
        public VGIS_Interaction AttachedHand = null;
        public VGIS_Interaction HoveringHand = null;
        public VGIS_Interaction Selectinghand = null;

        public List<Collider> Colliders = new List<Collider>();
        protected Vector3 ClosestHeldPoint;

        public virtual bool IsAttached
        {
            get
            {
                return AttachedHand != null;
            }
        }

        protected virtual void Awake()
        {
            if (Rigidbody == null)
                Rigidbody = this.GetComponent<Rigidbody>();

            if (Rigidbody == null && this.GetComponent<VGIS_InteractableItemSnapable>() == false)
            {
                gameObject.AddComponent<Rigidbody>();
                Rigidbody = this.GetComponent<Rigidbody>();
                //Debug.LogError("There is no rigidbody attached to this interactable.");
            }

            if (!GetComponent<VGIS_SnapManager>())
                Colliders.Add(this.GetComponent<Collider>());
        }

        protected virtual void Start()
        {
            if (!GetComponent<VGIS_SnapManager>())
                VGIS_Interactables.Register(this, Colliders);
            GetListOfMeshChildren(gameObject);
        }

        protected float lastPoses;
        protected float deltaPoses;
        public virtual void OnNewPosesApplied()
        {
            if (IsAttached == true)
            {
                if (AttachedHand.CurrentHandState == HandState.LaserInt)
                {
                    //float distance = Vector3.Distance(AttachedHand.transform.GetComponent<NVRExampleLaserPointer>().InteractionPoint.transform.position, transform.position);
                    //if(distance > DropDistance *1000)
                    //    DroppedBecauseOfDistance();
                    //todo: fix laser having higher drop distance
                }
                else
                {
                    float shortestDistance = float.MaxValue;

                    for (int index = 0; index < Colliders.Count; index++)
                    {
                        //todo: this does not do what I think it does.
                        Vector3 closest = Colliders[index].ClosestPointOnBounds(AttachedHand.transform.position);
                        float distance = Vector3.Distance(AttachedHand.transform.position, closest);

                        if (distance < shortestDistance)
                        {
                            shortestDistance = distance;
                            ClosestHeldPoint = closest;
                        }
                    }

                    if (shortestDistance > DropDistance)
                    {
                        // DroppedBecauseOfDistance();
                    }
                }
            }
            deltaPoses = Time.time - lastPoses;

            if (deltaPoses == 0)
                deltaPoses = Time.fixedDeltaTime;

            lastPoses = Time.time;
        }

        //Remove items that go too high or too low.
        protected virtual void Update()
        {
            if (this.transform.position.y > 10000 || this.transform.position.y < -10000)//Todo: fix this shit
            {
                if (AttachedHand != null)
                    AttachedHand.EndInteraction(this);

                Destroy(this.gameObject);
            }

        }

        public virtual void BeginInteraction(VGIS_Interaction hand)
        {
            AttachedHand = hand;

            if (DisableKinematicOnAttach == true)
            {
                Rigidbody.isKinematic = false;
            }

            lastPoses = Time.time;
        }


        public void ForceDetach()
        {
            if (AttachedHand != null)
                AttachedHand.EndInteraction(this);

            if (AttachedHand != null)
                EndInteraction();
        }

        public virtual void EndInteraction()
        {
            AttachedHand = null;
            ClosestHeldPoint = Vector3.zero;

            if (EnableKinematicOnDetach == true)
            {
                Rigidbody.isKinematic = true;
            }

            if (EnableGravityOnDetach == true)
            {
                Rigidbody.useGravity = true;
            }
        }

        public virtual void Hightlight(VGIS_Interaction hand)
        {
            List<Material> mats = new List<Material>();
            mats.AddRange(GetComponent<MeshRenderer>().materials);
            mats.Add(Instantiate((Material)Resources.Load("OutlineBasic")));
            mats.Last().color = hand.SelectionColor;

            GetComponent<MeshRenderer>().materials = mats.ToArray();
        }

        public virtual void UnHightlight(VGIS_Interaction hand)
        {
            List<Material> mats = new List<Material>();
            mats.AddRange(GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials);
            mats.Remove(mats.Last());

            GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials = mats.ToArray();
        }

        public virtual void StartHoverHightlight(VGIS_Interaction hand, Color color)
        {
            if (IsHoverHightlighted) return;
            IsHoverHightlighted = true;

            _listOfHoverMaterials.AddRange(GetComponent<MeshRenderer>().materials);
            foreach (Material material in _listOfHoverMaterials)
            {
                material.EnableKeyword("_EMISSION");
            }
            HighligtherEnumerator = HoverHightlighter(_listOfHoverMaterials, color);
            StartCoroutine(HighligtherEnumerator);
        }

        public virtual bool StopHoverHightlight(VGIS_Interaction hand, VGIS_Interactable newItem = null)
        {
            StopCoroutine(HighligtherEnumerator);
            foreach (Material hoverMaterial in _listOfHoverMaterials)
            {
                hoverMaterial.DisableKeyword("_EMISSION");
            }
            IsHoverHightlighted = false;
            return true;
        }

        protected virtual IEnumerator HoverHightlighter(List<Material> materials, Color color)
        {
            while (IsHoverHightlighted)
            {              
                foreach (Material material in materials)
                {
                    float emission = Mathf.PingPong(Time.time, 0.4f);
                    Color baseColor = color;
                    Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission) * 0.75f;
                    material.SetColor("_EmissionColor", finalColor);
                }
                yield return new WaitForEndOfFrame();
            }
        }

        public virtual bool BeginSelect(VGIS_Interaction hand)
        {
            Selectinghand = hand;
            IsSelected = true;
            Hightlight(hand);
            return true;
        }

        public virtual bool EndSelect(VGIS_Interactable newItem)
        {
            UnHightlight(Selectinghand);
            Selectinghand = null;
            IsSelected = false;
            return true;
        }


        protected virtual void DroppedBecauseOfDistance()
        {
            AttachedHand.EndInteraction(this);
        }

        protected virtual void OnDestroy()
        {
            ForceDetach();
            VGIS_Interactables.Deregister(this);
        }

        private void GetListOfMeshChildren(GameObject obj)
        {
            if (null == obj)
                return;

            foreach (Transform child in obj.transform)
            {
                if (null == child) continue;
                if (child.GetComponent<MeshRenderer>())
                {
                    _listOfRenderers.Add(child.GetComponent<MeshRenderer>());
                }
                GetListOfMeshChildren(child.gameObject);
            }
        }

    }
}
