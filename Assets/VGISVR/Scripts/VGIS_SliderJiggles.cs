﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using UnityEngine.UI;

public class VGIS_SliderJiggles : MonoBehaviour
{

    private Slider _slider;
    private InputField _field;
  

    void Awake()
    {
        if (GetComponent<Slider>())
            _slider = GetComponent<Slider>();
        if (GetComponent<InputField>())
            _field = GetComponent<InputField>();
    }

    public void UpdateValueFromSlider(InputField outputField)
    {
        outputField.text = _slider.value.ToString(CultureInfo.InvariantCulture);
    }

    public void UpdateValueFromInput(Slider outputSlider)
    {
        float temp = 0.0f;
        float.TryParse(_field.text, out temp);
        //float.TryParse(this.GetComponent<InputField>().text, out temp);
        outputSlider.value = temp;
    }
}
