﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using VGIS;
using VRTK.Highlighters;

public class VGIS_InteractableItemSnapable : VGIS_InteractableItem
{

    public VGIS_SnapManager Assembly;
    public MeshRenderer Renderer;

    public bool IsSnapped()
    {
        return Assembly.Parts[this.gameObject];
    }

    protected override void Start()
    {
        base.Start();
        Renderer = GetComponentInChildren<MeshRenderer>();
    }

    // Use this for initialization
    public void BeginSnap(VGIS_Interaction hand)
    {
        this.transform.SetParent(Assembly.transform);
        this.transform.localPosition = Assembly.OriginalPositions[this.gameObject];
        this.transform.localRotation = Quaternion.Euler(0, 0, 0);
        Destroy(this.transform.GetComponent<Rigidbody>());
        foreach (var collider1 in Colliders)
        {
            collider1.isTrigger = false;
        }
        if (Assembly.GetComponent<VGIS_Interactable>().IsSelected && !IsSelected)
            Hightlight(Assembly.GetComponent<VGIS_Interactable>().Selectinghand);
        Assembly.SetAttachedState(this, true);

    }

    public void EndSnap()
    {
        this.Rigidbody = transform.gameObject.AddComponent<Rigidbody>();
        this.Rigidbody.maxAngularVelocity = 100f;
        foreach (var collider1 in Colliders)
        {
            collider1.isTrigger = true;
        }
        this.transform.SetParent(null);
        if (Assembly.GetComponent<VGIS_Interactable>().IsSelected && !IsSelected) UnHightlight(Assembly.GetComponent<VGIS_Interactable>().Selectinghand);
        Assembly.SetAttachedState(this, false);
    }

    public override void Hightlight(VGIS_Interaction hand)
    {
        if (Assembly.Item.IsSelected && IsHightlighted)
        {
            List<Material> mats = new List<Material>();
            mats.AddRange(Renderer.materials);
            mats.Remove(mats.Last());
            mats.Add(Instantiate((Material)Resources.Load("OutlineBasic")));
            mats.Last().SetColor("g_vOutlineColor", hand.SelectionColor);
            Renderer.materials = mats.ToArray();
            IsHightlighted = true;
        }
        else
        {
            List<Material> mats = new List<Material>();
            mats.AddRange(Renderer.materials);
            mats.Add(Instantiate((Material)Resources.Load("OutlineBasic")));
            mats.Last().SetColor("g_vOutlineColor", hand.SelectionColor);
            Renderer.materials = mats.ToArray();
            IsHightlighted = true;
        }             
    }

    public override void UnHightlight(VGIS_Interaction hand)
    {
        if (Assembly.Parts[this.gameObject] && IsSelected)
        {
            List<Material> mats = new List<Material>();
            mats.AddRange(Renderer.materials);
            mats.Last().SetColor("g_vOutlineColor", Assembly.Item.HightlightColor);
            Renderer.materials = mats.ToArray();
            IsHightlighted = true;
        }
        else
        {
            List<Material> mats = new List<Material>();
            mats.AddRange(Renderer.materials);
            mats.Remove(mats.Last());
            Renderer.materials = mats.ToArray();
            IsHightlighted = false;
        }
    }

    public override void StartHoverHightlight(VGIS_Interaction hand, Color color)
    {
        CreateHandle();
        if (IsHoverHightlighted && hand == HoveringHand) return; //Allows single target override of multi target hightlight
        if (IsHoverHightlighted && HoveringHand != null && !HoveringHand.SelectEntireObject) return; // Stops the previous statement from going out of controls with 2 single target hands

        if (GetComponent<MeshRenderer>()) _listOfHoverMaterials.AddRange(GetComponent<MeshRenderer>().materials);
        _listOfHoverMaterials.AddRange(GetComponentInChildren<MeshRenderer>().materials);

        foreach (Material hoverMaterial in _listOfHoverMaterials)
        {
            hoverMaterial.EnableKeyword("_EMISSION");
        }
        HoveringHand = hand;
        HighligtherEnumerator = HoverHightlighter(_listOfHoverMaterials, color);
        IsHoverHightlighted = true;
        StartCoroutine(HighligtherEnumerator);
    }

    public override bool StopHoverHightlight(VGIS_Interaction hand, VGIS_Interactable newItem = null)
    {
        if (!IsHoverHightlighted) return false;
        if (HoveringHand != hand) return false;


        if (!Assembly.Item.IsHoverHightlighted)
        {
            foreach (Material hoverMaterial in _listOfHoverMaterials)
            {
                hoverMaterial.DisableKeyword("_EMISSION");
            }
        }
        

        IsHoverHightlighted = false;
        HoveringHand = null;
        StopCoroutine(HighligtherEnumerator);
        _listOfHoverMaterials.Clear();
        RemoveHandle();
        return true;
    }

    public override bool BeginSelect(VGIS_Interaction hand)
    {       
        Selectinghand = hand;
        Hightlight(hand);
        IsSelected = true;

        float scaleFactor = Vector3.Distance(this.transform.position,
                                VGIS_Player.Instance.transform.position) / 3;

        //Create menu
        Menu = Instantiate(Resources.Load("ItemMenu") as GameObject);
        Menu.GetComponent<VGIS_ItemMenu>().item = this;
        Menu.GetComponent<VGIS_ItemMenu>().renderer = Renderer;
        Menu.GetComponent<Canvas>().worldCamera = VGIS_ViveControllerInput.Instance.ControllerCamera;
        Menu.transform.localScale *= scaleFactor;
        Vector3[] corners = new Vector3[4];
        Menu.GetComponent<RectTransform>().GetWorldCorners(corners);
        float offset = (corners[1].y - corners[0].y) / 2;
        Menu.transform.position = transform.position + Vector3.up * offset +
                                  VGIS_Player.Instance.Head.transform.right *
                                  Renderer.bounds.extents.x +
                                  VGIS_Player.Instance.Head.transform.right * offset;
        Menu.transform.LookAt(VGIS_Player.Instance.Head.transform);
        LineRenderer line = Menu.AddComponent<LineRenderer>();

        line.SetPosition(0, corners[3]);
        line.SetPosition(1, this.transform.position);
        line.SetWidth(0.05f, 0.05f);
        return true;
    }

    public override bool EndSelect(VGIS_Interactable newItem)
    {      
        UnHightlight(Selectinghand);
        IsSelected = false;
        Selectinghand = null;
        Destroy(Menu);
        RemoveHandle();
        return true;
    }

    protected override void CreateHandle()
    {
        if (IsSelected) return;
        if (Handle) return;
        Handle = Instantiate(Resources.Load("3DHandle") as GameObject);
        Handle.transform.localScale = Vector3.zero;
        Handle.transform.position = Renderer.bounds.center;
        Handle.transform.forward = this.transform.forward;
        Handle.transform.SetParent(transform);
        Handle.transform.localEulerAngles = Vector3.zero;
    }

    protected override void RemoveHandle()
    {
        if (IsSelected || IsHoverHightlighted) return;
        Destroy(Handle);
    }
}
