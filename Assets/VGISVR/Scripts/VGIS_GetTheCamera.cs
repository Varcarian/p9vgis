﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class VGIS_GetTheCamera : MonoBehaviour
{

    private List<Canvas> _canvases;

    void Awake()
    {
        _canvases = GetComponentsInChildren<Canvas>().ToList();
    }

	// Use this for initialization
	void Start ()
	{
	    
	}
	
	// Update is called once per frame
	void Update () {

	    if (_canvases[0].worldCamera == null)
	    {
            foreach (Canvas canvas in _canvases)
            {
                canvas.worldCamera = VGIS_ViveControllerInput.Instance.ControllerCamera;
            }
        }       
    }
}
