﻿using UnityEngine;
using System.Collections;
using System;
using Valve.VR;

public class VGIS_MenuPlacer : MonoBehaviour
{

    public GameObject TargetObject;

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
        
    }
    private void OnNewPoses(TrackedDevicePose_t[] arg0)
    {
        transform.rotation = TargetObject.transform.rotation;
        transform.position = TargetObject.transform.position;
        Canvas.ForceUpdateCanvases();
    }
    void OnEnable()
    {
        SteamVR_Events.NewPoses.Listen(OnNewPoses);
    }
    void OnDisable()
    {
        SteamVR_Events.NewPoses.Remove(OnNewPoses);
    }
}
