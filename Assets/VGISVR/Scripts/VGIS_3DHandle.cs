﻿using UnityEngine;
using System.Collections;
using VGIS;

public class VGIS_3DHandle : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float scale = Vector3.Distance(VGIS_Player.Instance.Head.transform.position, transform.position)*2;
        transform.localScale = new Vector3(scale,scale,scale)/transform.root.localScale.x;
	}
}
