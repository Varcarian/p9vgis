﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using VGIS;

namespace VGIS
{

    public class VGIS_InteractableItem : VGIS_Interactable
    {
        [Tooltip("If you have a specific point you'd like the object held at, create a transform there and set it to this variable")]
        public Transform InteractionPoint;

        protected Transform PickupTransform;
        protected GameObject Menu;
        protected GameObject Handle;
        public Color HightlightColor { get; set; }

        public Bounds Bounds = new Bounds();
        public Vector3 WorldExtents = new Vector3();

        private List<GameObject> _listOfChildren = new List<GameObject>();

        protected override void Awake()
        {
            base.Awake();
            if (transform.GetComponent<Rigidbody>())
            {
                this.Rigidbody.maxAngularVelocity = 100f;
            }

            gameObject.layer = LayerMask.NameToLayer("Interactable");

            if (!this.GetComponent<VGIS_SnapManager>()) //This messes up snap items so dont do it
            {
                Bounds = CalculateLocalBoundsAndSetCenter();
                AdjustCollider();
                Rigidbody.freezeRotation = true;
                Rigidbody.isKinematic = true;
                DisableKinematicOnAttach = true;
                EnableKinematicOnDetach = true;
                // HACK : REMOVE THIS LATER
            }              
        }

        private Bounds CalculateLocalBoundsAndSetCenter()
        {

            GetListOfMeshChildren(this.gameObject);
            Quaternion currentRotation = this.transform.rotation;
            //transform.rotation = Quaternion.Euler(0f, 0f, 0f); //old
            Bounds bounds = new Bounds(this.transform.position, Vector3.zero);
            foreach (MeshRenderer rdr in GetComponentsInChildren<MeshRenderer>())
            {
                bounds.Encapsulate(rdr.bounds);
            }
            foreach (GameObject child in _listOfChildren)
            {
                child.transform.SetParent(null);
            }
            transform.position = bounds.center;
            foreach (GameObject child in _listOfChildren)
            {
                child.transform.SetParent(transform);
            }
            bounds.center = Vector3.zero;
            if (Math.Abs(transform.eulerAngles.Abs().y) > 0.001f
                && Math.Abs(transform.eulerAngles.Abs().y - 180) > 0.001f)
            {
                bounds.extents = new Vector3(bounds.extents.z, bounds.extents.y, bounds.extents.x); //HACK: Biggest hack i have ever done award #1
            }
            WorldExtents = bounds.extents;
            bounds.extents = bounds.extents.Divide(transform.localScale);
            //transform.rotation = currentRotation;      //old   

            return bounds;
        }

        public void AdjustCollider()
        {
            BoxCollider b = GetComponent<BoxCollider>();
            b.size = Bounds.extents * 2;           
        }

        public override void OnNewPosesApplied()
        {
            base.OnNewPosesApplied();

            if (IsAttached != true) return;

            Quaternion RotationDelta = new Quaternion();
            Vector3 PositionDelta = new Vector3();

            float angle;
            Vector3 axis;

            if (InteractionPoint != null)
            {
                if (AttachedHand.CurrentHandState == HandState.LaserInt)
                {
                    if (Rigidbody.isKinematic)
                    {
                        transform.position = AttachedHand.InteractionPoint.transform.position;
                    }
                    else
                    {
                        RotationDelta = AttachedHand.InteractionPoint.transform.rotation * Quaternion.Inverse(InteractionPoint.rotation);
                        PositionDelta = AttachedHand.InteractionPoint.transform.position - InteractionPoint.position;
                    }
                }
                else
                {
                    if (Rigidbody.isKinematic)
                    {
                        transform.position = AttachedHand.transform.position;
                    }
                    else
                    {
                        RotationDelta = AttachedHand.transform.rotation * Quaternion.Inverse(InteractionPoint.rotation);
                        PositionDelta = (AttachedHand.transform.position - InteractionPoint.position);
                    }
                }
            }
            else
            {
                if (Rigidbody.isKinematic)
                {
                    transform.position = PickupTransform.position;
                }
                else
                {
                    RotationDelta = PickupTransform.rotation * Quaternion.Inverse(this.transform.rotation);
                    PositionDelta = (PickupTransform.position - this.transform.position);
                }
            }

            if (!Rigidbody.isKinematic)
            {
                RotationDelta.ToAngleAxis(out angle, out axis);

                if (angle > 180)
                    angle -= 360;

                if (angle != 0)
                {
                    Vector3 AngularTarget = angle * axis;
                    this.Rigidbody.angularVelocity = Vector3.MoveTowards(this.Rigidbody.angularVelocity, AngularTarget, 10f * (deltaPoses * 1000));
                }

                Vector3 VelocityTarget = PositionDelta / deltaPoses;
                this.Rigidbody.velocity = Vector3.MoveTowards(this.Rigidbody.velocity, VelocityTarget, 10f);
            }

        }

        public override void BeginInteraction(VGIS_Interaction hand)
        {
            base.BeginInteraction(hand);
            gameObject.layer = LayerMask.NameToLayer("CurrentBox");
            if (hand.CurrentHandState == HandState.LaserInt)
            {
                PickupTransform = new GameObject(string.Format("[{0}] VGISPickupTransform", this.gameObject.name)).transform;
                PickupTransform.parent = hand.InteractionPoint.transform;
                PickupTransform.position = this.transform.position;
                PickupTransform.rotation = this.transform.rotation;
            }
            else
            {
                PickupTransform = new GameObject(string.Format("[{0}] VGISPickupTransform", this.gameObject.name)).transform;
                PickupTransform.parent = hand.transform;
                PickupTransform.position = this.transform.position;
                PickupTransform.rotation = this.transform.rotation;
            }

        }

        public override void EndInteraction()
        {
            base.EndInteraction();
            gameObject.layer = LayerMask.NameToLayer("Interactable");
            if (PickupTransform != null)
                Destroy(PickupTransform.gameObject);
        }

        public override void Hightlight(VGIS_Interaction hand)
        {
            IsHightlighted = true;
            HightlightColor = hand.SelectionColor;
            if (this.transform.root.GetComponent<VGIS_SnapManager>())
            {
                foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
                {
                    if (part.Value == false) continue;
                    if (part.Key.GetComponent<VGIS_InteractableItemSnapable>().IsSelected) continue;
                    List<Material> mats = new List<Material>();
                    mats.AddRange(part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials);
                    mats.Add(Instantiate((Material)Resources.Load("OutlineBasic")));
                    mats.Last().SetColor("g_vOutlineColor", hand.SelectionColor);
                    part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials = mats.ToArray();
                    part.Key.GetComponent<VGIS_InteractableItemSnapable>().IsHightlighted = true;
                }
            }
            else
            {      
                              
                if (_listOfChildren.Count == 0)
                {
                    hand.Hightlighter.AddObject(gameObject, new List<Renderer>() { GetComponent<Renderer>() });
                }
                else
                {
                    List<Renderer> rdrs = _listOfChildren.Select(x => x.GetComponent<Renderer>()).ToList();
                    hand.Hightlighter.AddObject(gameObject, rdrs);
                }
            }
        }

        public override void UnHightlight(VGIS_Interaction hand)
        {
            IsHightlighted = false;
            HightlightColor = new Color();
            if (this.GetComponent<VGIS_SnapManager>())
            {
                foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
                {
                    if (part.Value == false) continue;
                    if (part.Key.GetComponent<VGIS_InteractableItemSnapable>().IsSelected) continue;
                    List<Material> mats = new List<Material>();
                    mats.AddRange(part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials);
                    mats.Remove(mats.Last());
                    part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.materials = mats.ToArray();
                    part.Key.GetComponent<VGIS_InteractableItemSnapable>().IsHightlighted = false;
                }
            }
            else
            {
                if (hand == null) return;
                hand.Hightlighter.RemoveObject(gameObject);                  
            }
        }

        public override void StartHoverHightlight(VGIS_Interaction hand, Color color)
        {
            if (IsHoverHightlighted) return;
            if (!CanHoverHightlight) return;
            HoveringHand = hand;
            if (this.GetComponent<VGIS_SnapManager>())
            {
                foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
                {
                    if (part.Value == false) continue;
                    if (part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted) continue;
                    part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted = true;
                    part.Key.GetComponent<VGIS_Interactable>().HoveringHand = hand;
                    _listOfHoverMaterials.AddRange(part.Key.GetComponent<VGIS_InteractableItemSnapable>().Renderer.sharedMaterials);
                    foreach (Material hoverMaterial in _listOfHoverMaterials)
                    {
                        hoverMaterial.EnableKeyword("_EMISSION");
                        hoverMaterial.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
                    }
                }
            }
            else
            {
                if (IsHoverHightlighted) return;
                HoveringHand = hand;

                foreach (var render in GetComponentsInChildren<MeshRenderer>())
                {
                    _listOfHoverMaterials.AddRange(render.materials);
                    
                    foreach (Material material in render.materials)
                    {
                        material.EnableKeyword("_EMISSION");
                        material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
                    }
                }
            }
            HighligtherEnumerator = HoverHightlighter(_listOfHoverMaterials, color);
            IsHoverHightlighted = true;
            StartCoroutine(HighligtherEnumerator);
            CreateHandle();
        }

        public override bool StopHoverHightlight(VGIS_Interaction hand, VGIS_Interactable newItem = null)
        {
            if (!IsHoverHightlighted) return false;
            if (HoveringHand != hand) return false;

            if (this.GetComponent<VGIS_SnapManager>())
            {
                foreach (KeyValuePair<GameObject, bool> part in this.GetComponent<VGIS_SnapManager>().Parts)
                {
                    if (part.Value == false) continue;
                    if (!part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted) continue;
                    if (part.Key.GetComponent<VGIS_Interactable>().HoveringHand != hand) continue;
                    part.Key.GetComponent<VGIS_Interactable>().IsHoverHightlighted = false;
                    part.Key.GetComponent<VGIS_Interactable>().HoveringHand = null;
                    foreach (Material hoverMaterial in _listOfHoverMaterials)
                    {
                        hoverMaterial.DisableKeyword("_EMISSION");
                    }
                }
            }
            else
            {
                foreach (Material hoverMaterial in _listOfHoverMaterials)
                {                   
                    hoverMaterial.DisableKeyword("_EMISSION");
                    hoverMaterial.globalIlluminationFlags = MaterialGlobalIlluminationFlags.EmissiveIsBlack;
                }
            }
            IsHoverHightlighted = false;
            HoveringHand = null;
            StopCoroutine(HighligtherEnumerator);
            _listOfHoverMaterials.Clear();
            RemoveHandle();
            return true;
        }

        public override bool BeginSelect(VGIS_Interaction hand)
        {
            if (CanSelect)
            {
                IsSelected = true;
                Hightlight(hand);
                Selectinghand = hand;
                return true;
            }
            return false;

            ////Stuff for menus
            //float scaleFactor = Vector3.Distance(this.transform.position,
            //                        VGIS_Player.Instance.transform.position)/3;
            //if (!this.GetComponent<VGIS_SnapManager>())
            //{
            //    GetComponent<Rigidbody>().useGravity = false;
            //    //Create menu
            //    Menu = Instantiate(Resources.Load("ItemMenu") as GameObject);
            //    Menu.GetComponent<VGIS_ItemMenu>().item = this;
            //    Menu.GetComponent<VGIS_ItemMenu>().renderer = GetComponent<MeshRenderer>();
            //    Menu.GetComponent<Canvas>().worldCamera = VGIS_ViveControllerInput.Instance.ControllerCamera;
            //    Menu.transform.localScale *= scaleFactor;
            //    Vector3[] corners = new Vector3[4];
            //    Menu.GetComponent<RectTransform>().GetWorldCorners(corners);
            //    float offset = (corners[1].y - corners[0].y)/2;
            //    Menu.transform.position = transform.position + Vector3.up*offset +
            //                              VGIS_Player.Instance.Head.transform.right*
            //                              GetComponent<MeshRenderer>().bounds.extents.x +
            //                              VGIS_Player.Instance.Head.transform.right*offset;
            //    Menu.transform.LookAt(VGIS_Player.Instance.Head.transform);
            //    LineRenderer line = Menu.AddComponent<LineRenderer>();

            //    line.SetPosition(0, corners[3]);
            //    line.SetPosition(1, this.transform.position);
            //    line.SetWidth(0.05f, 0.05f);

            //}
        }

        public override bool EndSelect(VGIS_Interactable newItem)
        {
            IsSelected = false;
            GetComponent<Rigidbody>().useGravity = true;
            UnHightlight(Selectinghand);
            Selectinghand = null;           
            Destroy(Menu);
            RemoveHandle();
            return true;
        }

        protected virtual void CreateHandle()
        {
            if (IsSelected) return;
            if (!GetComponent<MeshRenderer>())
            {
                Handle = Instantiate(Resources.Load("3DHandle") as GameObject);
                Handle.transform.localScale = Vector3.zero;
                Handle.transform.position = transform.position;
                Handle.transform.forward = this.transform.forward;
                Handle.transform.SetParent(transform);
                Handle.transform.localEulerAngles = Vector3.zero;
            }
            else
            {
                Handle = Instantiate(Resources.Load("3DHandle") as GameObject);
                Handle.transform.localScale = Vector3.zero;
                Handle.transform.position = this.GetComponent<Renderer>().bounds.center;
                Handle.transform.forward = this.transform.forward;
                Handle.transform.SetParent(transform);
                Handle.transform.localEulerAngles = Vector3.zero;
            }

        }

        protected virtual void RemoveHandle()
        {
            if (IsSelected || IsHoverHightlighted) return;
            Destroy(Handle);
        }

        private void GetListOfMeshChildren(GameObject obj)
        {
            if (null == obj)
                return;

            foreach (Transform child in obj.transform)
            {
                if (null == child) continue;
                if (child.GetComponent<MeshRenderer>())
                {                  
                    _listOfChildren.Add(child.gameObject);
                }
                GetListOfMeshChildren(child.gameObject);
            }
        }
    }
}