﻿using System;
using UnityEngine;
using System.Collections;
using VGIS;
using VRTK;

public class VGIS_ThreeDRotator : MonoBehaviour
{

    public bool IsActive = false;
    public bool IsRotating = false;
    public float SnapAngleStart = 0;
    public float MininumSwipeDistance = 0.2f;
    public float MinimunSwipeVelocity = 4.0f;

    private VRTK_ControllerEvents _controllerEvents;

    private Vector2 _touchStart, _touchEnd;
    private float _touchTime, _touchAngle;
    public float TurningRate = 2f;
    public float CurrentTarget = 0;
    public float CurrentTurningRate = 0;
    public float TurnedSoFar = 0;
    
	// Use this for initialization
	void Start ()
	{
	    _controllerEvents = VGIS_Player.Instance.LeftHand.GetComponent<VRTK_ControllerEvents>();
	    _controllerEvents.TouchpadTouchStart += DoTouchpadTouchStart;
	    _controllerEvents.TouchpadTouchEnd += DoTouchpadTouchEnd;
	    _controllerEvents.TouchpadAxisChanged += DoTouchpadAxisChanged;
        transform.Rotate(new Vector3(0,SnapAngleStart,0), Space.Self);
	}
	
	// Update is called once per frame
	void Update ()
	{
	    
	}

    private void DoTouchpadTouchStart(object sender, ControllerInteractionEventArgs e)
    {
        _touchTime = Time.time;
        _touchStart = e.touchpadAxis;
    }
    private void DoTouchpadAxisChanged(object sender, ControllerInteractionEventArgs e)
    {
        _touchAngle = e.touchpadAngle;
        _touchEnd = e.touchpadAxis;
    }
    private void DoTouchpadTouchEnd(object sender, ControllerInteractionEventArgs e)
    {   
        CheckForSwipe();
    }

    private void CheckForSwipe()
    {
        float deltaTime = Time.time - _touchTime;
        float swipeVector = Vector2.Distance(_touchEnd,_touchStart);

        float velocity = swipeVector/deltaTime;
        if (velocity > MinimunSwipeVelocity && swipeVector > MininumSwipeDistance)
        {

            if (_touchAngle > 50 && _touchAngle < 130)
            {
                if (!IsRotating && gameObject.activeSelf)
                {
                    CurrentTarget = -120;
                    CurrentTurningRate = -TurningRate;
                    StartCoroutine(Rotator());
                    IsRotating = true;
                }
                else
                {
                    //Maybe add code here in future to interrupt current rotation
                }
            }
            else if (_touchAngle > 230 && _touchAngle < 310)
            {             
                if (!IsRotating && gameObject.activeSelf)
                {
                    CurrentTarget = 120;
                    CurrentTurningRate = TurningRate;
                    StartCoroutine(Rotator());
                    IsRotating = true;
                }
                else
                {
                    //Maybe add code here in future to interrupt current rotation
                }               
            }
        }
    }

    private IEnumerator Rotator()
    {
        while (Mathf.Abs(TurnedSoFar) < Mathf.Abs(CurrentTarget))
        {
            float turn = 0;
            if (Mathf.Abs(TurnedSoFar) + Mathf.Abs(CurrentTurningRate * Time.deltaTime) > 120)
            {
                if (TurnedSoFar < 0)
                    turn = -(120 + TurnedSoFar);
                else
                    turn = 120 - TurnedSoFar;
            }
            else
            {
                turn = CurrentTurningRate * Time.deltaTime;
            }
                
            transform.Rotate(new Vector3(0, turn, 0), Space.Self);
            TurnedSoFar += turn;
            yield return new WaitForEndOfFrame();
        }
        TurnedSoFar = 0;
        CurrentTarget = 0;
        CurrentTurningRate = 0;
        IsRotating = false;
    }

    private void OnDisable()
    {
        transform.Rotate(new Vector3(0, -TurnedSoFar, 0), Space.Self);
        TurnedSoFar = 0;
        CurrentTarget = 0;
        CurrentTurningRate = 0;
        IsRotating = false;
    }
}
