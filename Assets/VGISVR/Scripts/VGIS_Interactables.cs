﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace VGIS
{
    public class VGIS_Interactables
    {
        private static Dictionary<Collider, VGIS_Interactable> ColliderMapping;
        private static Dictionary<VGIS_Interactable, List<Collider>> VGISInteractableMapping;

        private static bool Initialized = false;

        public static void Initialize()
        {
            ColliderMapping = new Dictionary<Collider, VGIS_Interactable>();
            VGISInteractableMapping = new Dictionary<VGIS_Interactable, List<Collider>>();

            Initialized = true;
        }

        public static void Register(VGIS_Interactable interactable, List<Collider> colliders)
        {
            VGISInteractableMapping.Add(interactable, colliders);

            foreach (Collider t in colliders)
            {
                ColliderMapping.Add(t, interactable);
            }
        }

        public static void Deregister(VGIS_Interactable interactable)
        {
            VGIS_Player.DeregisterInteractable(interactable);

            ColliderMapping = ColliderMapping.Where(mapping => mapping.Value != interactable).ToDictionary(mapping => mapping.Key, mapping => mapping.Value);
            VGISInteractableMapping.Remove(interactable);
        }

        public static VGIS_Interactable GetInteractable(Collider collider)
        {
            VGIS_Interactable interactable;
            ColliderMapping.TryGetValue(collider, out interactable);
            return interactable;
        }
    }
}