﻿using System;
using UnityEngine;
using System.Collections;
using System.Text;
using UnityEngine.UI;
using Valve.VR;
using VRTK;

public abstract class VGIS_TextInput : MonoBehaviour
{
    public static VGIS_TextInput Instance; 
    public InputField TargetInputField;

    // Use this for initialization
    void Start ()
    {
        Instance = this;
	    SetupKeyboard();
	}
    //EventSystemManager.currentSystem
    #region Keyboard

    private void SetupKeyboard()
    {
        SteamVR_Events.System("KeyboardCharInput").Listen(OnKeyboard);
        SteamVR_Events.System("KeyboardClosed").Listen(OnKeyboardClosed);
    }

    private void OnKeyboard(VREvent_t arg0)
    {
        string derp = "";
        StringBuilder stringBuilder = new StringBuilder(256);
        SteamVR.instance.overlay.GetKeyboardText(stringBuilder, 256);

        if (stringBuilder.ToString() == "\b")
        {
            // User hit backspace
            if (TargetInputField.text.Length > 0)
            {
                TargetInputField.text = TargetInputField.text.Substring(0, TargetInputField.text.Length - 1);
            }
        }
        else if (stringBuilder.ToString() == "\x1b")
        {
            // Close the keyboard
            SteamVR.instance.overlay.HideKeyboard();
        }
        else
        {
            derp = stringBuilder.ToString();

        }
        try
        {
            TargetInputField.text += derp;
        }
        catch (NullReferenceException)
        {
            //Debug.LogError("NulRef was Caught");
        }

        // Do something with the accumulated text here
    }

    private void OnKeyboardClosed(VREvent_t arg0)
    {
        TargetInputField.MoveTextEnd(false);
        TargetInputField.DeactivateInputField();
        TargetInputField = null;
    }

    #endregion
}
