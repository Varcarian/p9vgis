﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using VRTK;

public class VGIS_Testing : MonoBehaviour
{
    //bug where the same radial button gets selected again and again.
    public List<Button> Buttons;
    public Button CurrentTarget;
    public TestType Type;
    public CanvasGroup Cnvas;
    public int ClicksThisOpening;
	// Use this for initialization
	void Start () {
	    
        StartCoroutine(LateStart(0.5f));
	}
    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        FindButtons(gameObject);
        Cnvas = GetComponent<CanvasGroup>();
        Cnvas.gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update () {
	
	}

    public void ShowCanvas()
    {
        //Cnvas.alpha = 1;
        //if(Type != TestType.Radial)
        //    Cnvas.blocksRaycasts = true;
        //Cnvas.interactable = true;
        Cnvas.gameObject.SetActive(true);
    }
    public void HideCanvas()
    {
        //Cnvas.alpha = 0;
        //Cnvas.blocksRaycasts = false;
        //Cnvas.interactable = false;
        Cnvas.gameObject.SetActive(false);
    }

    private void FindButtons(GameObject obj)
    {
        if (null == obj)
            return;

        foreach (Transform child in obj.transform)
        {
            if (null == child) continue;
            if (child.GetComponent<Button>())
            {
                Buttons.Add(child.GetComponent<Button>());
                var child1 = child;
                if (child.GetComponent<Button>().name.Contains("Arc"))
                {
                    child.parent.GetComponent<RadialMenu>().buttons[Buttons.Count-1].OnClick.AddListener(delegate { CallBack(child1.GetComponent<Button>()); });
                }
                else
                {
                    child.GetComponent<Button>().onClick.AddListener(delegate { CallBack(child1.GetComponent<Button>()); });
                }
                    
            }
            FindButtons(child.gameObject);
        }
    }

    void CallBack(Button btnPressed)
    {
        VGIS_TestingMaster instance = VGIS_TestingMaster.Instance;
        if (btnPressed == CurrentTarget)
        {
            
            instance.ClickCount += 1;
            instance.ClickList.Add(instance.TestTime + "," + "1");
            instance.UnhighlightButton(btnPressed);
            instance.GetRandomTarget(this);
            ClicksThisOpening += 1;
        }
        else
        {
            instance.ClickList.Add(instance.TestTime + "," + "0");
        }         
    }

    public void SetCurrentTarget(Button btn)
    {        
        CurrentTarget = btn;
        if(btn != null) VGIS_TestingMaster.Instance.HighlightButton(btn);
    }

    public void ClearButtons()
    {
        foreach (Button button in Buttons)
        {
            if (button.name.Contains("Arc"))
                button.GetComponent<UICircle>().color = Color.white;
            else
            {
                button.image.color = Color.white;
            }
        }
    }

}
