﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using VGIS;

namespace VGIS
{
    public class VGIS_InteractableItemTesting : VGIS_InteractableItem
    {

        public TestType type;
        public override bool BeginSelect(VGIS_Interaction hand)
        {
            if(gameObject != VGIS_TestingMaster.Instance.CurrentTarget)
                return false;

            IsSelected = true;
            Hightlight(hand);
            Selectinghand = hand;
            ShowMenu();
            VGIS_TestingMaster.Instance.GetRandomTarget(VGIS_TestingMaster.Instance.Menus.Single(p => p.Type == type));
            return true;

        }
        public override bool EndSelect(VGIS_Interactable newItem)
        {
            if (IsHightlighted) UnHightlight(Selectinghand);
            IsSelected = false;
            GetComponent<Rigidbody>().useGravity = true;
            Selectinghand.CurrentlySelecting = null;
            Selectinghand = null;
            
            RemoveHandle();
            HideMenu();
            return true;
        }
        public void ShowMenu()
        {
            VGIS_Testing menu = VGIS_TestingMaster.Instance.Menus.Single(p => p.Type == type);
            Canvas cnvs = menu.GetComponentInChildren<Canvas>();
            if (menu.Type == TestType.Flat)
            {
                float scaleFactor = Vector3.Distance(this.transform.position,
                         VGIS_Player.Instance.transform.position) / 3;

                Vector3[] corners = new Vector3[4];
                cnvs.GetComponent<RectTransform>().GetWorldCorners(corners);
                float offset = (corners[1].y - corners[0].y) / 2;
                menu.gameObject.transform.position = transform.position + Vector3.up * offset +
                                                     VGIS_Player.Instance.Head.transform.right *
                                                     GetComponent<MeshRenderer>().bounds.extents.x +
                                                     VGIS_Player.Instance.Head.transform.right * offset;
                //menu.transform.localScale *= scaleFactor;
                menu.transform.LookAt(VGIS_Player.Instance.Head.transform);
                //menu.transform.localPosition += new Vector3(0,0,0.2f); 

            }
            menu.ShowCanvas();
        }
        public void HideMenu()
        {
            VGIS_TestingMaster.Instance.Menus.Single(p => p.Type == type).HideCanvas();
            VGIS_TestingMaster.Instance.Menus.Single(p => p.Type == type).ClicksThisOpening = 0;
            VGIS_TestingMaster.Instance.Menus.Single(p => p.Type == type).ClearButtons();
        }
    }
}