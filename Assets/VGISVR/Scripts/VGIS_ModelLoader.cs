﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class VGIS_ModelLoader : MonoBehaviour
{
    public Shader shader;
    public Material standardMaterial;
    public Material transparentMaterial; 

    //private OBJImporter _importer = new OBJImporter();
    private string _mainFolderPath;
    public string FileName = "Reventon.obj";
    private GameObject[] _gO;
    private GameObject _empty;
    
	// Use this for initialization
	void Start ()
	{
	    standardMaterial = new Material(shader);
	    transparentMaterial = new Material(shader);
        _mainFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/Model";
    }
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.B))
	    {
            _empty = new GameObject(FileName.Split('.')[0]);
	        _gO = ObjReader.use.ConvertFile(_mainFolderPath + "/" + FileName, true, standardMaterial, transparentMaterial);
	        foreach (GameObject o in _gO)
	        {
	            o.transform.SetParent(_empty.transform);
	        }
	    }          
	}
}
