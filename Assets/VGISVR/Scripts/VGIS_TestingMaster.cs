﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine.UI;
using VGIS;
using VRTK;
using Random = UnityEngine.Random;

public class VGIS_TestingMaster : MonoBehaviour
{
    public static VGIS_TestingMaster Instance;
    public int ClicksPerTest = 10;
    public int ClickCount = 0;
    public List<VGIS_Testing> Menus;
    [Header("", order = 0)]
    public List<GameObject> Objects;
    [Header("", order = 1)]
    public GameObject CurrentTarget;
    [Header("", order = 2)]
    public GameObject LastGameObject;
    public int MenuNumber = 0;
    public List<TestType> TestOrder;
    public VGIS_Testing CurrentMenu;
    public int CurrentTest = 0;
    public List<GameObject> TeleporterLocations;
    public bool WaitingForNewTest = false;
    private bool IsInit = false;

    private VRTK_ControllerEvents _controllerEvents;
    private bool _writersOn = false;
    private StreamWriter _leftHandWriter;
    private StreamWriter _rightHandWriter;
    private StreamWriter _headWriter;
    private string _mainFolderPath;
    private string _currentFolderPath;
    public List<string> ClickList;
    private float _testStartTime;
    public float TestTime;
    private float _leftHandDistance = 0;
    private float _rightHandDistance = 0;
    private float _headDistance = 0;
    private float _leftHandRotation;
    private float _rightHandRotation;
    private float _headRotation;
    private Vector3 _lastlH;
    private Vector3 _lastrH;
    private Vector3 _lastH;
    private Quaternion _lastleftHandRotation;
    private Quaternion _lastrightHandRotation;
    private Quaternion _lastheadRotation;
    private const string Format = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}";

    public int ButtonCount()
    {
        int count = 0;
        foreach (VGIS_Testing menu in Menus)
        {
            count += menu.Buttons.Count;
        }
        return count;
    }
    // Use this for initialization
    void Start()
    {
        _mainFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\UnityTesting";
        _controllerEvents = VGIS_Player.Instance.RightHand.GetComponent<VRTK_ControllerEvents>();
        Instance = this;
        MakeFolders();
        _controllerEvents.TriggerClicked += DoTriggerClicked;
    }

    void DoTriggerClicked(object sender, ControllerInteractionEventArgs e)
    {
        if (!WaitingForNewTest)
            ClickList.Add(TestTime + "," + "2");
    }

    void OnDestroy()
    {
        CloseStreamWriters();
    }

    // Update is called once per frame
    void Update()
    {
        CalculateDistAndRots();

        if (Input.GetKeyDown(KeyCode.A))
        {
            RandomizeOrder();
            AssignObjectsType();
            SetCurrentTarget(GetRandomObject());
            StartLogging();
            //if (Menus[MenuNumber].CurrentTarget != null) UnhighlightButton(MenusZ[MenuNumber].CurrentTarget);
            //Menus[MenuNumber].CurrentTarget = GetRandomButton(Menus[MenuNumber]);
            //HighlightButton(Menus[MenuNumber].CurrentTarget);
        }
        if (Input.GetKeyDown(KeyCode.Space) && WaitingForNewTest)
        {
            SetCurrentTarget(GetRandomObject());
            WaitingForNewTest = false;
            StartLogging();
        }
    }

    private void StartLogging()
    {
        _leftHandDistance = 0;
        _rightHandDistance = 0;
        _headDistance = 0;
        _leftHandRotation = 0;
        _rightHandRotation = 0;
        _headRotation = 0;

        _testStartTime = Time.time;
        string fileName0 = @"\LeftHandData" + CurrentTest + ".csv";
        string fileName1 = @"\RightHandData" + CurrentTest + ".csv";
        string fileName2 = @"\HeadData" + CurrentTest + ".csv";
        _leftHandWriter = new StreamWriter(_currentFolderPath + fileName0);
        _rightHandWriter = new StreamWriter(_currentFolderPath + fileName1);
        _headWriter = new StreamWriter(_currentFolderPath + fileName2);
        _writersOn = true;
        WriteHeaders();
        StartCoroutine(FixedLogging());
    }

    private void CloseStreamWriters()
    {
        if (_leftHandWriter != null)
        {
            _writersOn = false;
            _leftHandWriter.Close();
            _headWriter.Close();
            _rightHandWriter.Close();
        }
    }

    private IEnumerator FixedLogging()
    {
        while (_writersOn)
        {
            yield return new WaitForSeconds(0.1f);
            WriteFiles();
        }
        CloseStreamWriters();
    }

    private void WriteClicks()
    {
        string path = _currentFolderPath + @"\ClickData" + (CurrentTest - 1) + ".csv";
        ClickList.Insert(0, TestOrder[CurrentTest - 1].ToString());
        File.WriteAllLines(path, ClickList.ToArray());
        ClickList.Clear();
    }

    private void WriteFiles()
    {
        if (_writersOn == false) return;
        TestTime = Time.time - _testStartTime;
        Vector3 lHP = VGIS_Player.Instance.LeftHand.transform.position;
        Vector3 lHR = VGIS_Player.Instance.LeftHand.transform.localEulerAngles;
        Vector3 rHp = VGIS_Player.Instance.RightHand.transform.position;
        Vector3 rHR = VGIS_Player.Instance.RightHand.transform.localEulerAngles;
        Vector3 HP = VGIS_Player.Instance.Head.transform.position;
        Vector3 HR = VGIS_Player.Instance.Head.transform.parent.localEulerAngles;

        _leftHandWriter.WriteLine(Format, TestTime, lHP.x, lHP.y, lHP.z, lHR.x, lHR.y, lHR.z, _leftHandDistance, _leftHandRotation, 0);
        _leftHandWriter.Flush();
        _rightHandWriter.WriteLine(Format, TestTime, rHp.x, rHp.y, rHp.z, rHR.x, rHR.y, rHR.z, _rightHandDistance, _rightHandRotation, 0);
        _rightHandWriter.Flush();
        _headWriter.WriteLine(Format, TestTime, HP.x, HP.y, HP.z, HR.x, HR.y, HR.z, _headDistance, _headRotation, 0);
        _headWriter.Flush();
    }

    private void WriteHeaders()
    {
        _leftHandWriter.WriteLine(Format, "Time", "Position X", "Position Y", "Position Z", "Rotation X", "Rotation Y", "Rotation Z", "Distance Traveled", "Angle Change", TestOrder[CurrentTest]);
        _leftHandWriter.Flush();
        _rightHandWriter.WriteLine(Format, "Time", "Position X", "Position Y", "Position Z", "Rotation X", "Rotation Y", "Rotation Z", "Distance Traveled", "Angle Change", TestOrder[CurrentTest]);
        _rightHandWriter.Flush();
        _headWriter.WriteLine(Format, "Time", "Position X", "Position Y", "Position Z", "Rotation X", "Rotation Y", "Rotation Z", "Distance Traveled", "Angle Change", TestOrder[CurrentTest]);
        _headWriter.Flush();
    }

    private void CalculateDistAndRots()
    {
        Vector3 lH = VGIS_Player.Instance.LeftHand.transform.position;
        Vector3 rH = VGIS_Player.Instance.RightHand.transform.position;
        Vector3 h = VGIS_Player.Instance.Head.transform.position;
        Quaternion lR = VGIS_Player.Instance.LeftHand.transform.localRotation;
        Quaternion rR = VGIS_Player.Instance.RightHand.transform.localRotation;
        Quaternion hR = VGIS_Player.Instance.Head.transform.parent.rotation;

        if (lH != Vector3.zero)
        {
            _leftHandDistance += Vector3.Distance(_lastlH, lH);
            _rightHandDistance += Vector3.Distance(_lastrH, rH);
            _headDistance += Vector3.Distance(_lastH, h);
            _leftHandRotation += Quaternion.Angle(_lastleftHandRotation, lR);
            _rightHandRotation += Quaternion.Angle(_lastrightHandRotation, rR);
            _headRotation += Quaternion.Angle(_lastheadRotation, hR);
        }

        _lastlH = lH;
        _lastrH = rH;
        _lastH = h;
        _lastleftHandRotation = lR;
        _lastrightHandRotation = rR;
        _lastheadRotation = hR;
    }

    private void MakeFolders()
    {
        if (Directory.Exists(_mainFolderPath))
        {
            var directories = Directory.GetDirectories(_mainFolderPath).ToList();

            if (directories.Count == 0)
            {
                _currentFolderPath = _mainFolderPath + @"\" + (directories.Count);
                Directory.CreateDirectory(_currentFolderPath);
            }
            else
            {
                var files = Directory.GetFiles(directories.Last()).ToList();

                if (files.Count > 0)
                {
                    _currentFolderPath = _mainFolderPath + @"\" + (directories.Count);
                    Directory.CreateDirectory(_currentFolderPath);
                }
                else
                {
                    _currentFolderPath = _mainFolderPath + @"\" + (directories.Count - 1);
                }
            }

        }
        else
        {
            Directory.CreateDirectory(_mainFolderPath);
        }
    }

    public void AssignObjectsType()
    {
        int divider = Objects.Count / TestOrder.Count;
        int testNumber = 0;
        for (int i = 0; i < Objects.Count; i++)
        {
            if (i % divider == 0 && i != 0)
            {
                testNumber += 1;
            }
            Objects[i].GetComponent<VGIS_InteractableItemTesting>().type = TestOrder[testNumber];
        }
    }

    public void RandomizeOrder()
    {
        List<TestType> test = MakeEnumList();

        Shuffle(test);

        TestOrder = test;
    }

    public static void Shuffle<T>(IList<T> list)
    {
        RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
        int n = list.Count;
        while (n > 1)
        {
            byte[] box = new byte[1];
            do provider.GetBytes(box);
            while (!(box[0] < n * (Byte.MaxValue / n)));
            int k = (box[0] % n);
            n--;
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public List<TestType> MakeEnumList()
    {
        var types = Enum.GetValues(typeof(TestType)).Cast<TestType>().ToList();

        return types;
    }

    public void SetCurrentTarget(GameObject target)
    {
        if (CurrentTarget != null)
        {
            CurrentTarget.GetComponent<Renderer>().material.color = Color.white;
            if (CurrentTarget.GetComponent<VGIS_InteractableItemTesting>().Selectinghand != null)
                CurrentTarget.GetComponent<VGIS_InteractableItemTesting>().EndSelect(null);
        }

        CurrentTarget = target;
        target.GetComponent<Renderer>().material.color = Color.red;
    }

    public void HighlightButton(Button button)
    {
        if (button.name.Contains("Arc"))
            button.GetComponent<UICircle>().color = Color.yellow;
        else
        {
            button.image.color = Color.yellow;
        }

    }

    public void UnhighlightButton(Button button)
    {
        if (button.name.Contains("Arc"))
            button.GetComponent<UICircle>().color = Color.white;
        else
        {
            button.image.color = Color.white;
        }
    }

    public void GetRandomTarget(VGIS_Testing menu)
    {
        if (ClickCount >= Menus.Count * ClicksPerTest)
        {
            if (CurrentTarget != null)
            {
                CurrentTarget.GetComponent<Renderer>().material.color = Color.white;
            }
            Debug.Log("Test: " + CurrentTest + " Done!");
            Debug.Log("All tests done");
            CloseStreamWriters();
            CurrentTest += 1;
            WriteClicks();
        }
        else if (ClickCount >= ClicksPerTest * (CurrentTest + 1))
        {
            Debug.Log("Test: " + CurrentTest + " Done!");
            CurrentTest += 1;
            VGIS_Player.Instance.gameObject.transform.position = TeleporterLocations[CurrentTest].transform.position;

            menu.SetCurrentTarget(null);
            menu.HideCanvas();
            WaitingForNewTest = true;
            CloseStreamWriters();
            WriteClicks();
        }
        else
        {
            int typeThreshold = 75;
            int random = Random.Range(0, 101);
            if ((random > typeThreshold || CurrentTarget == null) && menu.ClicksThisOpening > 1)
            {
                SetCurrentTarget(GetRandomObject());
                menu.SetCurrentTarget(null);
            }
            else
            {
                menu.SetCurrentTarget(GetRandomButton(menu));
            }
        }
    }

    public Button GetRandomButton(VGIS_Testing menu)
    {
        Button newButton = menu.Buttons[Random.Range(0, menu.Buttons.Count)];
        if (newButton == menu.CurrentTarget)
        {
            newButton = GetRandomButton(menu);
        }
        return newButton;
    }

    public GameObject GetRandomObject()
    {
        GameObject newObject = Objects[Random.Range(0, Objects.Count)];
        if (newObject == LastGameObject || newObject.GetComponent<VGIS_InteractableItemTesting>().type != TestOrder[CurrentTest])

            newObject = GetRandomObject();
        LastGameObject = newObject;
        return newObject;
    }

}

public enum TestType
{
    Radial,
    Flat,
    ThreeD
}
