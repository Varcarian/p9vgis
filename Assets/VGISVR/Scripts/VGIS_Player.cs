﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VGIS
{
    public class VGIS_Player : MonoBehaviour
    {
        public static VGIS_Player Instance;
        public bool PhysicalHands = false;
        public bool MakeControllerInvisibleOnInteraction = false;

        public VGIS_Head Head;
        public VGIS_Interaction LeftHand;
        public VGIS_Interaction RightHand;

        [HideInInspector]
        public VGIS_Interaction[] Hands;

        private Dictionary<Collider, VGIS_Interaction> ColliderToHandMapping;

        private void Awake()
        {
            Instance = this;
            VGIS_Interactables.Initialize();

            if (Head == null)
            {
                Head = this.GetComponentInChildren<VGIS_Head>();
            }

            if (LeftHand == null || RightHand == null)
            {
                Debug.LogError("[FATAL ERROR] Please set the left and right hand to a nvrhands.");
            }

            if (Hands == null || Hands.Length == 0)
            {
                Hands = new[] { LeftHand, RightHand };
            }

            ColliderToHandMapping = new Dictionary<Collider, VGIS_Interaction>();
        }

        public void RegisterHand(VGIS_Interaction hand)
        {
            Collider[] colliders = hand.GetComponentsInChildren<Collider>();

            foreach (Collider t in colliders)
            {
                if (ColliderToHandMapping.ContainsKey(t) == false)
                {
                    ColliderToHandMapping.Add(t, hand);
                }
            }
        }

        public VGIS_Interaction GetHand(Collider collider)
        {
            return ColliderToHandMapping[collider];
        }

        public static void DeregisterInteractable(VGIS_Interactable interactable)
        {
            foreach (VGIS_Interaction t in Instance.Hands)
            {
                t.DeregisterInteractable(interactable);
            }
        }
    }
}