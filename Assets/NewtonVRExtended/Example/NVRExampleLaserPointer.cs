﻿using UnityEngine;
using System.Collections;

namespace NewtonVR.Example
{
    public class NVRExampleLaserPointer : MonoBehaviour
    {
        public Color LineColor;
        public float LineWidth = 0.005f;
        public RaycastHit Hit;
        public GameObject StartPoint;
        public GameObject InteractionPoint;

        public bool LaserVisible = false;

        private LineRenderer Line;
        private EasyController ectrl;
        private NVRHand hand;
        private void Awake()
        {
            StartPoint = new GameObject("LaserStart " + transform.name);
            StartPoint.transform.SetParent(this.transform.GetComponentInChildren<Transform>());
            StartPoint.transform.rotation = this.transform.rotation * Quaternion.Euler(57, 0, 0);
            StartPoint.transform.localPosition = new Vector3(0, -0.0669f, 0.0345f);

            Line = this.GetComponent<LineRenderer>();
            InteractionPoint = new GameObject("LaserInteract " + transform.name);
            InteractionPoint.transform.SetParent(StartPoint.transform);
            InteractionPoint.transform.localPosition = new Vector3(0, 0, 0);
            InteractionPoint.transform.rotation = StartPoint.transform.rotation;

            ectrl = GetComponent<EasyController>();
            hand = GetComponent<NVRHand>();

            //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //cube.transform.SetParent(InteractionPoint.transform);
            //cube.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            //cube.transform.localPosition = Vector3.zero;
            //cube.layer = LayerMask.NameToLayer("Ignore Raycast");
            //DestroyImmediate(cube.transform.GetComponent<Collider>());

            if (Line == null)
            {
                Line = this.gameObject.AddComponent<LineRenderer>();
            }

            if (Line.sharedMaterial == null)
            {
                Line.material = new Material(Shader.Find("Unlit/Color"));
                Line.material.SetColor("_Color", LineColor);
                Line.SetColors(LineColor, LineColor);
            }

            Line.useWorldSpace = true;

        }

        private void LateUpdate()
        {
            Line.enabled = LaserVisible;//&& Hand != null;

            if (Line.enabled == true)
            {
                Line.material.SetColor("_Color", LineColor);
                Line.SetColors(LineColor, LineColor);
                Line.SetWidth(LineWidth, LineWidth);

                RaycastHit hitInfo;
                bool hit = Physics.Raycast(this.transform.GetComponentInChildren<Transform>().position, StartPoint.transform.forward, out hitInfo, 1000);
                Vector3 endPoint;

                if (hit == true && hand.CurrentlyInteracting == null)
                {
                    InteractionPoint.transform.rotation = this.transform.rotation;
                    endPoint = hitInfo.point;
                    if (hand.CurrentHandState != HandState.LaserOnInteracting)
                    {
                        InteractionPoint.transform.position = endPoint;
                    }
                    Hit = hitInfo;
                }
                else if (hand.CurrentlyInteracting != null)
                {
                    float value = (InteractionPoint.transform.localPosition.z + ectrl.deltaPadScroll < 0)
                        ? 0
                        : InteractionPoint.transform.localPosition.z + ectrl.deltaPadScroll;
                    InteractionPoint.transform.localPosition = new Vector3(InteractionPoint.transform.localPosition.x, InteractionPoint.transform.localPosition.y, value);
                    endPoint = InteractionPoint.transform.position;
                }
                else
                {
                    InteractionPoint.transform.rotation = this.transform.rotation;
                    endPoint = StartPoint.transform.position + (StartPoint.transform.forward * 1000f);
                    InteractionPoint.transform.position = endPoint;
                    Hit = new RaycastHit();
                }




                Line.SetPositions(new Vector3[] { StartPoint.transform.position, endPoint });
            }
        }
    }
}